<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Usuarios extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function clientes($x = '',$y = ''){
            $crud = $this->crud_function($x, $y);
            $crud->field_type('password','password')
                     ->field_type('ubicacion','map',array('width'=>'300px','height'=>'300px'))
                     ->field_type('lat','hidden')
                     ->field_type('lon','hidden');
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                $crud->field_type('status','dropdown',array('1'=>'Desconectado','2'=>'Conectado'));
                $crud->field_type('foto','image',array('path'=>'img/clientes','width'=>'200px','height'=>'200px'));
            }else{
                $crud->set_field_upload('foto','img/clientes');
                $crud->field_type('status','dropdown',array('1'=>'<span class="label label-default">Desconectado</span>','2'=>'<span class="label label-success">Conectado</span>'));
            }
            $crud->display_as('clientes_nombre','Nombre');
            $crud->columns('foto','clientes_nombre','email','telefono','status');
            $crud->set_rules('email','Email','required|valid_email');
            $crud->unset_delete();
            $output = $crud->render();            
            $output->title = 'Clientes';
            $this->loadView($output);
        }
        
        function repartidores($x = '',$y = ''){
            $crud = $this->crud_function($x, $y);
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                $crud->field_type('status','dropdown',array('1'=>'Desconectado','2'=>'Esperando Trabajo','3'=>'En Tránsito','4'=>'Siniestrado','5'=>'Bloqueado'));
                $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'200px','height'=>'200px'));
            }else{
                $crud->set_field_upload('foto','img/fotos');
                $crud->field_type('status','dropdown',
                        array(
                            '1'=>'<span class="label label-default">Desconectado</span>',
                            '2'=>'<span class="label label-success">Esperando Trabajo</span>',
                            '3'=>'<span class="label label-warning">En Tránsito</span>',
                            '4'=>'<span class="label label-danger">Siniestrado</span>',
                            '5'=>'<span class="label label-default">Bloqueado</span>'
                        ));
            }
            $crud->display_as('repartidores_nombre','Nombre');
            $crud->columns('foto','repartidores_nombre','email','telefono','status');
            $crud->field_type('password','password')
                     ->field_type('ubicacion','map',array('width'=>'300px','height'=>'300px'))
                     ->field_type('lat','hidden')
                     ->field_type('lon','hidden');       
            $crud->unset_delete();
            $output = $crud->render();            
            $output->title = 'Repartidores';
            $this->loadView($output);
        }
    }
?>
