<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Inventario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function productos($x = '',$y = ''){
            $crud = $this->crud_function($x, $y);
            $crud->display_as('productos_nombre','Nombre')
                     ->display_as('categorias_id','Categoria');
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){                
                $crud->field_type('foto_portada','image',array('path'=>'img/fotos','width'=>'200px','height'=>'200px'));
            }else{
                $crud->set_field_upload('foto_portada','img/fotos');                
            }
            $crud->columns('foto_portada','categorias_id','productos_nombre','precio');
            $crud->unset_delete();
            $output = $crud->render();
            $output->title = 'Productos';
            $this->loadView($output);
        }
        
        function ventas($x = '',$y = ''){
            $this->load->library('multicrud');
            $ventas = $this->crud_function('','',$this);
            $ventas->set_theme('crud_horizontal');
            $ventas->set_subject('Ventas');            
           if($ventas->getParameters()=='add' || $ventas->getParameters()=='edit'){
                $ventas->field_type('status','dropdown',array('-1'=>'Rechazado por el banco','-2'=>'Cancelado por el cliente','1'=>'Esperando pago','2'=>'Pagado','3'=>'En Tránsito','4'=>'Entregado'));
            }else{
                $ventas->field_type('status','dropdown',
                        array(
                            '-2'=>'<span class="label label-danger">Rechazado por el banco</span>',
                            '-1'=>'<span class="label label-danger">Cancelado por el cliente</span>',
                            '1'=>'<span class="label label-default">Esperando pago</span>',
                            '2'=>'<span class="label label-success">Pagado</span>',
                            '3'=>'<span class="label label-warning">En Tránsito</span>',
                            '4'=>'<span class="label label-info">Entregado</span>',                            
                        ));
            }
            
            $ventas->field_type('ubicacion','hidden')
                     ->field_type('lat','hidden')
                     ->field_type('lon','hidden');
            
            $ventas->display_as('clientes_id','Cliente')
                         ->display_as('repartidores_id','Repartidor')
                         ->display_as('id','#Pedido');
            $ventas->columns('id','clientes_id','repartidores_id','direccion_envio','total','status');
            $ventas->callback_after_update(function($post,$primary){
                if($post['status']>1 && $post['status']<4 && !empty($post['repartidores_id'])){
                    get_instance()->db->update('repartidores',array('status'=>3),array('id'=>$post['repartidores_id']));
                }
            });
            $ventas->unset_delete();
            $this->multicrud->addCrud($ventas);
            
            $ventas_detalles = new ajax_grocery_CRUD();                        
            $ventas_detalles->set_theme('crud_dependency');
            $ventas_detalles->set_subject('Detalles');
            $ventas_detalles->set_table('ventas_detalles');
            $ventas_detalles->set_relation('productos_id','productos','productos_nombre');
            $ventas_detalles->dependency = array('ventas_id'=>'ventas.id');
            $ventas_detalles->display_as('productos_id','Producto');
            $this->multicrud->addCrud($ventas_detalles);                        
            $crud = $this->multicrud->render();
            $crud->crud = 'ventas';
            $crud->title = 'Ventas';
            $this->loadView($crud);            
        }
        
        function categorias($x = '',$y = ''){
            $crud = $this->crud_function($x, $y);
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){                
                $crud->field_type('foto','image',array('path'=>'img/categorias','width'=>'200px','height'=>'200px'));
            }else{
                $crud->set_field_upload('foto','img/categorias');
            }          
            $crud->display_as('categorias_nombre','Nombre');
            $crud->unset_delete();
            $output = $crud->render();
            $output->title = 'Categorias';
            $this->loadView($output);
        }                
    }
?>
