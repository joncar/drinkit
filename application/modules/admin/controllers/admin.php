<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function ventasespera(){
            $this->as = array('ventasespera'=>'ventas');
            $crud = $this->crud_function('','');
            $crud->set_subject('ventas en espera');
            $crud->columns('clientes_id','repartidores_id','status','total');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->where('ventas.status','2');
            $crud->field_type('status','dropdown',
                        array(
                            '-2'=>'<span class="label label-danger">Rechazado por el banco</span>',
                            '-1'=>'<span class="label label-danger">Cancelado por el cliente</span>',
                            '1'=>'<span class="label label-default">Esperando pago</span>',
                            '2'=>'<span class="label label-success">Pagado</span>',
                            '3'=>'<span class="label label-warning">En Tránsito</span>',
                            '4'=>'<span class="label label-info">Entregado</span>',                            
                        ));
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function ventastransito(){
            $this->as = array('ventastransito'=>'ventas');
            $crud = $this->crud_function('','');
            $crud->set_subject('ventas en transito');
            $crud->where('ventas.status','3');
            $crud->columns('clientes_id','repartidores_id','status','total');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->field_type('status','dropdown',
                        array(
                            '-2'=>'<span class="label label-danger">Rechazado por el banco</span>',
                            '-1'=>'<span class="label label-danger">Cancelado por el cliente</span>',
                            '1'=>'<span class="label label-default">Esperando pago</span>',
                            '2'=>'<span class="label label-success">Pagado</span>',
                            '3'=>'<span class="label label-warning">En Tránsito</span>',
                            '4'=>'<span class="label label-info">Entregado</span>',                            
                        ));
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function ventascompletado(){
            $this->as = array('ventascompletado'=>'ventas');
            $crud = $this->crud_function('','');
            $crud->set_subject('ventas completados');
            $crud->where('ventas.status','4');
            $crud->columns('clientes_id','repartidores_id','status','total');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->field_type('status','dropdown',
                        array(
                            '-2'=>'<span class="label label-danger">Rechazado por el banco</span>',
                            '-1'=>'<span class="label label-danger">Cancelado por el cliente</span>',
                            '1'=>'<span class="label label-default">Esperando pago</span>',
                            '2'=>'<span class="label label-success">Pagado</span>',
                            '3'=>'<span class="label label-warning">En Tránsito</span>',
                            '4'=>'<span class="label label-info">Entregado</span>',                            
                        ));
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function clientesonline(){
            $this->as = array('clientesonline'=>'clientes');
            $crud = $this->crud_function('','');            
            $crud->set_subject('Clientes');
            $crud->columns('clientes_nombre','email','celular','ultima_conexion')
                     ->display_as('clientes_nombre','Cliente');
            $crud->where("ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5",null,TRUE);
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function repartidoresonline(){
            $this->as = array('repartidoresonline'=>'repartidores');
            $crud = $this->crud_function('','');         
            $crud->set_subject('Repartidores');
            $crud->columns('repartidores_nombre','email','celular','ultima_conexion')
                     ->display_as('repartidores_nombre','Repartidor');
            $crud->where("ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5",null,TRUE);
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
    }
?>
