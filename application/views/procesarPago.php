<!Doctype html>
<html lang="es">
	<head>
                <title><?= empty($title)?'Pizzasapp':$title ?></title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <script src="http://code.jquery.com/jquery-1.10.0.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
                <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
        </head>
        <body>
            <form onsubmit="return crearToken()">
                <div class="form-group">
                  <label for="email">Email Tarjeta Habiente</label>
                  <input type="email" class="form-control" id="email" placeholder="user@ejemplo.com" required="">
                </div>
                <div class="form-group">
                  <label for="nombre">Nombre Tarjeta Habiente</label>
                  <input type="text" class="form-control" id="nombre" placeholder="Nombre Tarjeta Habiente" required="">
                </div>
                <div class="form-group">
                  <label for="tarjeta">Numero de Tarjeta</label>
                  <input type="number" class="form-control" id="tarjeta" placeholder="Numero de tarjeta" required="">
                </div>
                <div class="form-group">
                  <label for="cvc">CVC</label>
                  <input type="number" class="form-control" id="cvc" placeholder="Numero de tarjeta" required="">
                </div>
                <div class="form-group">
                  <label for="mes">Mes Expiración</label>
                  <input type="number" class="form-control" id="mes" placeholder="Mes de Expiración" required="">
                </div>
                <div class="form-group">
                  <label for="ano">Año de expiración</label>
                  <input type="number" class="form-control" id="ano" placeholder="Año de expiración" required="">
                </div>
                <button type="submit" class="btn btn-default" id="process">Procesar Pago</button>
                <div class='alert alert-success' style='display:none;'>El pagó se registro satisfactoriamente, <a href='javascript:window.close()' class='btn btn-warning'>Cerrar Ventana</a></div>
            </form>
        </body>
        <script>
            function crearToken(){
                var publishable_key = 'key_GuXm7tsQztxyFAH6ykeiXaA'; //set your publishable key here
                Conekta.setPublishableKey(publishable_key);
                /* Los parámetros pueden ser un objeto de javascript, una forma de HTML o una forma de JQuery */
                var errorResponseHandler, successResponseHandler, tokenParams;
                tokenParams = {
                  "card": {
                    "number": $("#tarjeta").val(),
                    "name": $("#nombre").val(),
                    "exp_year": $("#ano").val(),
                    "exp_month": $("#mes").val(),
                    "cvc": $("#cvc").val()
                  }
                };
                $("#process").attr('disabled',true);
                /* Después de tener una respuesta exitosa, envía la información al servidor */
                successResponseHandler = function(token) {
                    $.post('<?= base_url('api/procesarPago/'.$id) ?>',{id:token.id,email:$("#email").val()},function(data){
                        if(data=='success'){
                            $(".alert").show();
                        }
                        else{
                            alert(data);
                            $("#process").removeAttr('disable');
                        }
                    });                    
                };

                /* Después de recibir un error */
                errorResponseHandler = function(error) {
                  alert(error.message);
                  $("#process").removeAttr('disable');
                };

                /* Tokenizar una tarjeta en Conekta */
                Conekta.token.create(tokenParams, successResponseHandler, errorResponseHandler);
    
                return false;
            }
        </script>
</html>
