Estatus del servidor: <?php echo exec('forever list'); ?>

<a href="javascript:reboot()" type="button" class="btn btn-danger">Reiniciar Servidor</a>
<script>
    function reboot(){
        if(confirm("Estas seguro que deseas reiniciar el core, puedes perder pedidos en espera, sincronización con los repartidores, etc.")){
            $.post('<?= base_url('seguridad/core/reboot') ?>',{},function(data){
                alert(data);
            });
        }
    }
</script>