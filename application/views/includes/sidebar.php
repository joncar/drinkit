<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'inventario'=>array('categorias','productos','ventas'),
                        'usuarios'=>array('clientes','repartidores'),
                        'admin'=>array('ajustes'),
                        'seguridad'=>array('grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'inventario'=>array('Inventario','fa fa-glass'),
                        'usuarios'=>array('Usuarios','fa fa-users'),
                        'admin'=>array('Admin','fa fa-lock'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>    
             <li>
                <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa  fa-mobile"></i>
                        <span class="menu-text">Test Apps</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li><a href="<?= base_url('main/appCliente') ?>">Cliente</a></li>
                </ul>
            </li>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
