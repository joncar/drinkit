<?php 
    require_once APPPATH.'/libraries/ajax_grocery_crud.php';      
    $crud = new ajax_grocery_CRUD();
    $crud->set_table('repartidores');
    $crud->set_theme('bootstrap2');
    $crud->set_subject('Ranking de repartidores hoy');    
    $crud->set_field_upload('foto','images/repartidores');
    $crud->unset_add()
            ->unset_edit()
            ->unset_read()
            ->unset_delete()
            ->unset_export()
            ->unset_print();
    $crud->columns('foto','nombre_repartidor','Total Cobrado','Propinas','Total a entregar','meta');
    
    $crud->callback_column('Total Cobrado',function($val,$row){
         $task = getTareas($val,$row);
         $valor = $task[1]->num_rows>0?$task[1]->row()->precio_por_paquete:0;
         return '$'.$task[0]*$valor;
    });
    
    $crud->callback_column('Propinas',function($val,$row){
         $task = getTareas($val,$row);         
         return '$'.$task[0]*10;
    });
    
    $crud->callback_column('Total a entregar',function($val,$row){
         $task = getTareas($val,$row);
         $valor = $task[1]->num_rows>0?$task[1]->row()->precio_por_paquete:0;
         $total = $task[0]*$valor;                  
         $propinas = $task[0]*10;
         $monto = ($total-$propinas);
         $class = $monto>0?'success':'default';
         $str = '<span class="label label-'.$class.'">$'.$monto.'</span>';
         return $str;
    });
    
    $crud->callback_column('meta',function($val,$row){
       $val = getTareas($val,$row)[0];
       if($val>=0 && $val<=10){
           $color = 'danger';
       }
       if($val>10 && $val<=20){
           $color = 'warning';
       }
       if($val>21){
           $color = 'success';
       }
       $str = '
       <div class="progress">
        <div class="progress-bar progress-bar-'.$color.'" role="progressbar" aria-valuenow="'.$val.'" aria-valuemin="0" aria-valuemax="25" style="width: '.round((($val*100)/25),2).'%;">
          '.$val.'
        </div>
      </div>';
       return $str;
    });
    $output = $crud->render();
    echo $output->output;
?>