<div id="page-wrapper" style="min-height: 722px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-dashboard"></i> Administración</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>        
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-clock-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="espera">0</h2>
                                <div>Pedidos en espera</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('admin/ventasespera') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-paper-plane fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="transito">0</h2>
                                <div>Pedidos en tránsito</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('admin/ventastransito') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-check fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="completados">0</h2>
                                <div>Pedidos Completados</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('admin/ventascompletado') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="clientes">0</h2>
                                <div>Clientes <br/>Online!</div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:mostrar('<?= base_url('admin/clientesonline') ?>')">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
             <div class="col-lg-2 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-motorcycle fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge" id="repartidores">0</h2>
                                <div>Repartidores Online!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <a href="javascript:mostrar('<?= base_url('admin/repartidoresonline') ?>')">
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" align="right">
                                <i class="fa fa-money fa-2x"></i>
                            </div>
                            <div class="col-xs-12 text-right">
                                <h2 class="huge" id="ventas">0</h2>
                                <div>Ventas concretadas!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div>

                    <!-- Mapas -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#home" id="mapaenvivoa" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Mapa en vivo</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div id="mapaenvivo" style="height:600px; width:100%">
                                Mapa
                            </div>
                        </div>                        
                    </div>                                        
                </div>                 
                <!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bell fa-fw"></i> Transacciones
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="list-group" id="ventasList">                              
                            </div>
                        <!-- /.list-group -->
                        <a class="btn btn-default btn-block" href="<?= base_url('sucursal/transacciones') ?>">Ver todos</a>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->
    </div>
<div id="info" style="display:none">
    <div class="col-xs-2">
        <img src="" id="foto" style="width:100%">
    </div>
    <div class="col-xs-10">
        <div id="nombre"></div>
        <div id="email"></div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?signed_in=true&v=3.exp&sensor=true"></script>
<?php $this->load->view('predesign/datepicker') ?>
<script>
    //Inicializar mapa
    <?php $coords_default = $this->db->get('ajustes')->row(); ?>        
    var mapaContent = document.getElementById('mapaenvivo');
    var center = new google.maps.LatLng<?= $coords_default->mapa ?>;    
    var mapOptions = {
                zoom: <?= $coords_default->zoom ?>,
                center: center,               
    };
    var map = new google.maps.Map(mapaContent, mapOptions);
    var repartidores = [];
    var ventas = [];
    var centrado = 0;
    var repartidoresBD = <?php 
        $re = array();
        foreach($this->db->get_where('repartidores',array('repartidores.status <='=>5))->result() as $r){
            $re[$r->id] = $r;
        }
        echo json_encode($re);
    ?>;             
        
     var marcadores = [];
    
    // Objeto repartidor
    function repartidor(){
        this.mark = undefined;
        this.position = undefined;
        this.info = undefined;
        this.id = undefined;
        this.addEvents = function(){
            var r = this;            
            google.maps.event.addListener(this.mark,'click',function(){
                r.info.open(r.map,r.mark);
            });
        };
        this.move = function(position){
            this.mark.setPosition(position);
        }
        this.remove = function(){
            this.mark.setMap(null);
        }        
    }
    function moverMapa(data){
        var bounds = new google.maps.LatLngBounds();
        for(var i in data){
             bounds.extend(new google.maps.LatLng(data[i].lat,data[i].lon));
        }
        map.fitBounds(bounds);
    }
    
    function mostrar(url){
        $.post(url,{},function(data){
            emergente(data);
        });
    }
    
    /*** Mostrarlos todos ****/
    for(var i in repartidoresBD){
            l = repartidoresBD[i];
            crearRepartidor(l);
    };
    
    function crearRepartidor(l){
        r = new repartidor();
        r.id = l.id;
        r.map = map;
        r.position = new google.maps.LatLng(l.lat,l.lon);
        r.mark = new google.maps.Marker({
            position: r.position,
            map: r.map,
            icon:'<?= base_url('img/transport-4.png') ?>'
         });
        r.info =  new google.maps.InfoWindow();
        $("#info #foto").attr('src','<?= base_url('img/fotos') ?>/'+l.foto);
        $("#info #nombre").html(l.nombre_repartidor);
        $("#info #email").html(l.email);
        r.info.setContent($("#info").html());
        r.addEvents();
        repartidores.push(r);
        marcadores.push({lat:l.lat,lon:l.lon});
    }
    
    function crearVenta(l){
        r = new repartidor();
        r.id = l.id;
        r.map = map;
        r.position = new google.maps.LatLng(l.lat,l.lon);
        r.mark = new google.maps.Marker({
            position: r.position,
            map: r.map,
            icon:'<?= base_url('img/restaurant-2.png') ?>'
         });
        r.info =  new google.maps.InfoWindow();        
        $("#info #nombre").html('Envio #'+l.id);
        $("#info #email").html(l.email);
        r.info.setContent($("#info").html());
        r.addEvents();
        ventas.push(r);        
    }
    moverMapa(marcadores);
    
    function mostrar(url){
        $.post(url,{},function(data){
            emergente(data);
        });
    }
    
    function update(){
        $.post('panel/dashboard',{},function(data){
            data = JSON.parse(data);
            $("#espera").html(data.pedidosEnEspera);
            $("#transito").html(data.pedidosEnTransito);
            $("#completados").html(data.pedidosCompletados);
            $("#repartidores").html(data.repartidoresOnline);
            $("#clientes").html(data.clientesOnline);
            $("#ventas").html('$ '+data.ventas);
            //Mover repartidores
            var enc = false;
            for(var i in data.repartidoresList){
                //Existe?
                for(var k in repartidores){
                    if(repartidores[k].id===data.repartidoresList[i].id){
                        //Lo movemos
                        var d = data.repartidoresList[i];
                        repartidores[k].mark.setPosition(new google.maps.LatLng(d.lat,d.lon));
                        if(d.online){
                            repartidores[k].mark.setIcon('<?= base_url('img/transport.png') ?>');
                        }else{
                            repartidores[k].mark.setIcon('<?= base_url('img/transport-4.png') ?>');
                        }
                        enc = true;
                    }
                }
                //no existe lo creamos
                if(!enc){
                    crearRepartidor(d);
                }
            }
            
            $("#ventasList").html('');
            for(i in data.ventasList){
                var v = data.ventasList[i];
                var str = '<a class="list-group-item" href="<?= base_url('inventario/ventas/edit/') ?>/'+v.id+'">';
                       str+= '<i class="fa fa-comment fa-fw"></i> Envio #'+v.id+" "+v.clientes_nombre+' hizo una compra por $'+v.total;
                       str+= '<span class="pull-right text-muted small"><em>'+v.fecha_solicitud+'</em>';
                       str+= '</span>';
                       str+= '</a>';
               $("#ventasList").append(str);
                enc = false;
                //Existe?
                 for(var k in ventas){
                     if(ventas[k].id===v.id){
                         //Lo movemos
                         if(v.status==='4'){
                             ventas[k].mark.setIcon('<?= base_url('img/restaurant-2.png') ?>');
                         }else{                             
                            ventas[k].mark.setIcon('<?= base_url('img/restaurant-3.png') ?>');
                        }
                         enc = true;
                     }
                 }
                 //no existe lo creamos
                 if(!enc){
                     crearVenta(v);
                 }
            }
            setTimeout(update,10000);
        });        
    }
    update();
</script>