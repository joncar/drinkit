<form method="post" action="<?= base_url('sucursal/estado_cuenta/'.$x.'/print') ?>">
    <div class="form-group col-xs-6">
            <label class="control-label col-xs-3">Desde: </label>
            <div class="col-xs-9">
                <input type="text" name="fechadesde" placeholder="dd-mm-yy" class="form-control datetime-input" id="desde" required="">
            </div>
        </div>
        <div class="form-group col-xs-6">
            <label class="control-label col-xs-3">Hasta: </label>
            <div class="col-xs-9">
                <input type="text" name="fechahasta" placeholder="dd-mm-yy" class="form-control datetime-input" id="hasta" required="">
            </div>
        </div>
        <div class="col-xs-12" align="center">
            <button type="submit" class="btn btn-success">Imprimir Estado de cuenta</button>            
        </div>
</form>
<?php $this->load->view('predesign/datepicker') ?>