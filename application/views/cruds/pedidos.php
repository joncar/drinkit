<?= $output[0]->output ?>
<div id="pedidosDetalles" class="hidden"><?= $output[1]->output ?></div>
<script>
    $(document).on('ready',function(){
       $("#status_field_box").after('<div id="detalles_field_box" class="form-group">'+$("#pedidosDetalles").html()+'</div>');        
        setTimeout(update,30000);
    });        
    
    function update(){
        console.log('update');        
        //$(".ajax_refresh_and_loading").trigger('click');        
        $(".filtering_form").trigger('submit');
        setTimeout(update,30000);
    }
    
    function notificarSucursal(sucursal,repartidor,pedidos_id,foto,placa,nombre,rank,tiempoDeRespuesta){
        if(confirm('Seguro que desea enviarle esta alerta al cliente?. Posiblemente ya la haya recibido')){
            var data = {
                sucursal:sucursal,
                repartidor:repartidor,
                pedidos_id:pedidos_id,
                foto:foto,
                placa:placa,
                nombre:nombre,
                rank:rank,
                tiempoDeRespuesta:tiempoDeRespuesta
            };        
            ws.emit('webResponderSolicitud',data);
            alert('Alerta de repartidor asignado enviada');
        }
    }
    
    function notificarRepartidor(pedidos_id,repartidor){
        if(confirm('Seguro que desea enviarle esta alerta al repartidor?. Posiblemente ya la haya recibido')){
            var data = {                
                repartidor:repartidor,
                idPedido:pedidos_id,                
            };        
            ws.emit('webBuscarRepartidor',data);
            alert('Alerta de repartidor asignado enviada');
        }
    }
</script>