<?php 
$total = 0;
$total2 = 0;
$total3 = 0;
foreach($estado->result() as $e){
    $total+= $e->monto/1.16;
    $total2+= $e->precio;
    $total3+= $e->monto;
}
?>
<html>
    <head>
    <meta charset="utf-8">
    </head>
    <body>
            <table cellpadding="10" cellspacing="10" style="width:1024px">
              <tbody>
                  <tr>
                      <td align="center" style="width:612px;"><h1>Estado de cuenta</h1></td>
                      <td align="center" style="width:612px"><h1>Balance</h1></td>
                  </tr>
                <tr>
                <td>
                    <table style="width:612px;">
                    <tr>
                        <td rowspan="6" style="width:100px;"><img src="<?= base_url('img/logo.png') ?>" width="100px;"></td>          
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>          
                      <td><strong>Direccion </strong> Eucalipto # 22 Torre AB-2306</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><strong>Colonia </strong> Jesús del Monte, Huixquilucan</td>
                    </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>Estado de México, México   CP 52764</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align='center'><strong>www.deliverymix.com.mx</strong></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align='center'><strong>info@deliverymix.com.mx</strong></td>
                    </tr>        
                  </table>  
                </td>
                <td>
                    <table>
                        <tr>
                          <td>Cobro por paquetes:</td>          
                          <td>$<?= number_format($total2,2,',','.') ?></td>
                        </tr>
                        <tr>
                            <td>Costo de envíos:</td>
                            <td>$<?= number_format($total,2,',','.') ?></td>
                        </tr>
                        <tr>
                            <td>Iva de envíos:</td>
                            <td>$<?= number_format($total*0.16,2,',','.') ?></td>
                        </tr>
                         <tr>
                            <td>Total</td>
                            <td>$<?= number_format($total2-$total-($total*0.16),2,',','.') ?></td>
                        </tr>            
                  </table>  
                </td>
              </tr>
            </tbody>
            </table>

            <table style="width:1024px;">
                <tr>
                    <td><strong>Cliente No. </strong><?= $estado->row()->sucursalid ?> </td>
                    <td>Estado de cuenta del: <?= $fechadesde ?> Al: <?= $fechahasta ?></td>
                </tr>
                 <tr>
                     <td style="width:612px;"><strong>Nombre</strong> <?= $estado->row()->nombre_sucursal ?></td>
                    <td style="width:612px;"><strong>Teléfono</strong> <?= $estado->row()->telefono_fijo ?></td>
                </tr>
                 <tr>
                     <td><strong>Dirección </strong>  <?= $estado->row()->direccion ?></td>
                     <td><strong> Email </strong>  <?= $estado->row()->email ?></td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Ciudad</strong> <?= $estado->row()->ciudad ?></td>        
                </tr>
            </table>
            <h1 align="center"><i>Estado de Cuenta Detallado</i></h1>
            <table style="width:1024px;">
                <thead>
                    <tr>
                        <th style="width:10%">Fecha</th>
                        <th style="width:10%">No. de envío</th>
                        <th style="width:10%">Nombre</th>
                        <th style="width:30%">Dirección</th>
                        <th style="width:10%">Cobro por paquete</th>
                        <th style="width:10%">Comisión 5%</th>
                        <th style="width:10%">Costo por envío</th>
                        <th style="width:10%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $subtotal = 0 ?>
                    <?php foreach($estado->result() as $e): ?>
                        <tr>
                            <td style="width:10%"><?= date("d-m-Y H:i:s",strtotime($e->fecha)) ?></td>
                            <td style="width:10%"><?= $e->pedidoid ?></td>
                            <td style="width:10%"><?= $e->cliente ?></td>
                            <td style="width:30%"><?= $e->direccion_entrega ?></td>
                            <td style="width:10%">$<?= $e->precio ?></td>
                            <td style="width:10%">$<?= $e->precio*0.05 ?></td>
                            <td style="width:10%">$<?= number_format($e->monto/1.16,2,',','.') ?></td>
                            <td style="width:10%">$<?= number_format(($e->precio-($e->precio*0.05))-($e->monto/1.16),2,',','.') ?></td>
                            <?php $subtotal+= round(($e->precio-($e->precio*0.05))-($e->monto/1.16),2); ?>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td colspan="7" align="right">Sub-Total</td>
                        <td>$<?= number_format($total2-$total,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" align="right">IVA 16% de envíos</td>
                        <td>$<?= number_format($total*0.16,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" align="right">Total</td>
                        <td>$<?= number_format($total2-$total-($total*0.16),2,',','.'); ?></td>
                    </tr>
                </tbody>
            </table>
        
    </body>
</html>