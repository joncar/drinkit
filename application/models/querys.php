<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getPedidosEnEspera(){
        return $this->db->get_where('ventas',array('status'=>2))->num_rows;        
    }
    function getPedidosEnTransito(){
        return $this->db->get_where('ventas',array('status'=>3))->num_rows;        
    }
    function getPedidosCompletados(){
        return $this->db->get_where('ventas',array('status'=>4))->num_rows;        
    }
    function getVentas(){
        return $this->db->query('Select SUM(total) as total from ventas where status=4')->row()->total;        
    }
    function getClientesOnline(){
        return $this->db->query("SELECT * from clientes where ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5");
    }
    function getRepartidoresOnline(){
        return $this->db->query("SELECT * from repartidores where ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5");
    }
    function getVentasList(){
        $this->db->limit('10');
        $this->db->order_by('ventas.id','DESC');
        $list = array();
        $this->db->select('ventas.id, clientes.clientes_nombre, clientes.email, ventas.total, ventas.status, ventas.fecha_solicitud, ventas.lat, ventas.lon');
        $this->db->join('clientes','clientes.id = ventas.clientes_id');
                
        foreach($this->db->get("ventas")->result() as $r){            
            $list[] = $r;
        }
        return $list;
    }
    
    function getRepartidoresList(){
        $list = array();                
        foreach($this->db->query("SELECT * from repartidores")->result() as $r){
            $r->online = false;
            foreach($this->getRepartidoresOnline()->result() as $o){
                if($o->id === $r->id){
                    $r->online = true;
                }
            }
            $list[] = $r;
        }
        return $list;
    }
    
    function get_resumenes(){
        $resumenes = array();
        $resumenes['pedidosEnEspera'] = $this->getPedidosEnEspera();
        $resumenes['pedidosEnTransito'] = $this->getPedidosEnTransito();
        $resumenes['pedidosCompletados'] = $this->getPedidosCompletados();
        $resumenes['ventas'] = $this->getVentas();
        $resumenes['clientesOnline'] = $this->getClientesOnline()->num_rows;
        $resumenes['repartidoresOnline'] = $this->getRepartidoresOnline()->num_rows;
        $resumenes['repartidoresList'] = $this->getRepartidoresList();
        $resumenes['ventasList'] = $this->getVentasList();
        return $resumenes;
    }
}