<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url());
                    exit;
                }
                
                if(empty($_SESSION['filtros'])){
                    $_SESSION['filtros']['fechadesde'] = date("Y-m-01");
                }
        }
        
        public function loadView($data)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            if(!$this->user->hasAccess()){
                throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
            }
            else{
                if(!empty($data->output)){
                    $data->view = empty($data->view)?'panel':$data->view;
                    $data->crud = empty($data->crud)?'user':$data->crud;
                    $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
                }
                parent::loadView($data);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
                $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }                
                
        public function dashboard(){
            $resumenes = $this->querys->get_resumenes();
            echo json_encode($resumenes);
        }
        
        public function aplicarfiltros($clean = ''){            
            if(!empty($_POST['fechadesde'])){
                if(empty($_SESSION['filtros'])){
                    $_SESSION['filtros'] = array();
                }
                $_SESSION['filtros']['fechadesde'] = $_POST['fechadesde'];
            }
            if(!empty($_POST['fechahasta'])){
                if(empty($_SESSION['filtros'])){
                    $_SESSION['filtros'] = array();
                }
                $_SESSION['filtros']['fechahasta'] = $_POST['fechahasta'];
            }
            if(!empty($clean)){
                if(!empty($_SESSION['filtros'])){
                    unset($_SESSION['filtros']);
                }
            }
            header("Location:".base_url('panel'));
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
