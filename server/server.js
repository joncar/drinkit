var server = require('http').createServer();
var io = require('socket.io')(server);
var email   = require("emailjs");
var db = require('db_driver');

io.set( 'origins', '*:*' );
var refresco_lista_espera = 60*1; //1 minutos
var max_intentos_lista_espera = 10; //limite de intentos para poder buscar repartidor en listas de espera, si este valor llega a 10 se anulará el envió. y se deberá solicitar el repartidor de manera manual.
var tiempo_para_rankear = 60*10; //10 minutos luego de que se haga la entrega para recordar a la sucursal que rankee al repartidor.
var tiempo_asignacion_espera_respuesta = 60*2; //2 minutos
//Conectar BD
var pizzasdb = require('pizzasdb');
var chat = require('chat');
var Group = require('GrupoClass').Group;
var clientes = new Group();
var repartidores = new Group();
var web = new Group();
var envios = new Group();
var solicitudes = new Group();
var lista_espera = new Group();
var timerSecundero = undefined;
var timerRealTime = undefined;

pizzasdb.correo = function(title,message){
    pizzasdb.getAjustes(function(){
        email.correo(pizzasdb.ajustes.email_logs,title,message);
    });
};
console.log('Servidor Iniciado');

io.sockets.on('connection', function (cliente) {
    cliente.emit('onopen','');    
    cliente.on('connectID',function(id){        
        //Asignar ID
        cliente.id = id.id;
        cliente.type = id.type;
        if(cliente.type==='sucursal'){
            if(clientes.getFromId(id.id)===null){
                clientes.add(cliente);
            }
            else{
                clientes.update(cliente);
            }
             pizzasdb.conectarSucursal(cliente.id);
        }
        else if(cliente.type==='web'){
            if(web.getFromId(cliente.id)==null){
                web.add(cliente);
            }
        }
        else{            
            pizzasdb.conectarRepartidor(id.id);
            if(repartidores.getFromId(id.id)==null){
                repartidores.add(cliente);
                //Consultar si hay pedidos en lista de espera para pedirlos
                if(lista_espera.list.length>0){
                    lista_espera.list[0].intentos-=1;
                    buscarRepartidor(lista_espera.list[0].cliente,lista_espera.list[0].idPedido);
                }
            }
            else{
                repartidores.update(cliente);
            }        
        }
    });
    
    //Funciones Particulares, borrar cuando se reutilice el codigo en otro proyecto
    cliente.on('buscarRepartidor',function(idPedido){
        buscarRepartidor(cliente,idPedido);
    });
    
    //Programar envios
    cliente.on('programar',function(idPedido){
        reintentarLuego(idPedido,cliente,idPedido.time);        
        console.log('Envio programado');
    });
    //Aqui es cuando el repartidor acepta o rechaza la solicitud
    cliente.on('responderSolicitud',function(data){
        console.log(data);
        responderSolicitud(cliente,data);
    });
    
    //Enviar un mensaje
    cliente.on('sendMessage',function(data){
        if(data.pedido_id!==undefined && data.dest!==undefined){
            var grupo = cliente.type==='sucursal'?repartidores:clientes;
            var destinatario = grupo.getFromId(data.dest);
            console.log(destinatario);
            chat.addMessage(data);
            if(destinatario!==null){
                destinatario.emit('message_receiver',data);
            }
        }
    });
    
    //Notificar que el envio fue entregado
    cliente.on('finishEntrega',function(data){
        c = clientes.getFromId(data.sucursales_id);
        if(c!==null){
            c.emit('finishEntrega',data);
            lista_espera.add({id:data.pedidos_id, time:tiempo_para_rankear, cliente:c,type:'rankear',repartidores_id:data.repartidores_id});
        }                
    });
    
    //Mover las coordenadas
    cliente.on('updateCoords',function(data){               
       cliente.lat = data.lat;
       cliente.lon = data.lon;
       cliente.nombre = data.nombre;
    });
    
    //Web notifica al cliente quien es su repartidor
    cliente.on('webResponderSolicitud',function(data){   
        console.log(data);
        var c = clientes.getFromId(data.sucursal);
        if(c!==null){
            console.log(c.id);
            c.emit('respuestaSolicitud',{response:true,data:data,tiempoDeRespuesta:data.tiempoDeRespuesta,repartidor:data.repartidor}); 
        }
    });
    
   cliente.on('webBuscarRepartidor',function(idPedido){
       console.log(idPedido);
       c = repartidores.getFromId(idPedido.repartidor); 
       if(c!==null){
            c.emit('onElegido',{idPedido:idPedido.idPedido});            
        }
   });
    
    cliente.on('disconnect',function(reasonCode, description){        
        switch(cliente.type){
            case 'sucursal': 
                pizzasdb.desconectarSucursal(cliente.id);
                clientes.remove(cliente); 
            break;
            case 'web':                 
                web.remove(cliente); 
            break;
            default: 
                console.log('Repartidor desconectado');
                pizzasdb.desconectarRepartidor(cliente.id);
                repartidores.remove(cliente);
                var s = solicitudes.getFromId(cliente.id);
                if(s!==null){
                    responderSolicitud(cliente,{response:false,idPedido:s.idPedido});
                }
            break;
        }
    });
    
    cliente.on('changeDate',function(date){
        timeCorte = date.date;
    });
    
});

function responderSolicitud(cliente,data){
    if(data.response){
            var listaespera = lista_espera.getFromId(data.pedidos_id);
            if(listaespera!=null){
                lista_espera.remove({id:data.pedidos_id});
            }
            //Traemos el pedido para verificar si lo tomo alguien
            db.callback = function(rows){     
                console.log(rows);
                if(rows.length>0 && rows[0].repartidores_id===null){
                    console.log(rows[0].repartidores_id);
                    db.clean();                    
                    var cl = clientes.getFromId(rows[0].sucursales_id);
                    cl = cl || {id:rows[0].sucursales_id};
                    asignarRepartidorOffLine(cl,{idPedido:data.pedidos_id},cliente.id);
                }else{
                    if(parseInt(rows[0].repartidores_id)!==parseInt(cliente.id)){
                        cliente.emit('message_receiver',{message:'La solicitud ya ha sido tomada por otro repartidor.'});
                    }
                }
            };
            db.get_where('pedidos',{id:data.pedidos_id});                                
    }
}

function buscarRepartidor(cliente,idPedido){    
    console.log('Buscando repartidores');
    db.callback = function(row){                
       if(row.length>0 && row[0].repartidores_id===null){
           if(repartidores.list.length>0){
                for(var i in repartidores.list){
                    repartidores.list[i].emit('onElegido',{idPedido:idPedido.idPedido});
                }
                lista_espera.add({id:idPedido.idPedido,idPedido:idPedido, time:tiempo_asignacion_espera_respuesta, cliente:cliente,intentos:0,type:'asignarRepartidorOffLine',block:false});
            }else{
                asignarRepartidorOffLine(cliente,idPedido);
            }
       } 
    };
    db.where('id',idPedido.idPedido);
    db.get('pedidos');    
}

 function asignarRepartidorOffLine(cliente,idPedido,filtrar){
    filtrar = filtrar || 0;
    console.log(filtrar);
    //Asignar al desconectado con menos tareas
    db.callback = function(result){
        var data = {
            sucursal:cliente.id,
            repartidor:result[0].id,
            pedidos_id:idPedido.idPedido,
            foto:'http://74.208.12.230/pizzasapp/images/repartidores/'+result[0].foto,
            placa:result[0].placa,
            nombre:result[0].nombre_repartidor,
            rank:result[0].calificacion,
            tiempoDeRespuesta:((parseInt(result[0].tareas)*20)+20)
        };
        pizzasdb.addRepartidor(idPedido.idPedido,0,result[0].id,'',data.tiempoDeRespuesta);
        if(typeof(cliente.emit)!=='undefined'){
            cliente.emit('respuestaSolicitud',{response:true,data:data,tiempoDeRespuesta:data.tiempoDeRespuesta,repartidor:result[0].id});
        }
    };
    db.select('repartidores.id, repartidores.foto, repartidores.placa, repartidores.nombre_repartidor, repartidores.calificacion, count(pedidos_detalles.id) as tareas');
    db.join('pedidos','pedidos.repartidores_id = repartidores.id');
    db.join('pedidos_detalles','pedidos_detalles.pedidos_id = pedidos.id');
    if(filtrar===0){
        db.where('pedidos.status <',3);
        db.where('repartidores.status <=',5);
    }else{
        db.where('repartidores.id',filtrar);
    }
    db.group_by('repartidores.id');
    db.order_by('tareas','asc');
    db.get('repartidores');
    console.log('Repartidor no encontrado, Buscando repartidores offlines');
}

function reintentarLuego(idPedido,cliente,time){
    time = time===undefined?refresco_lista_espera:time;
    if(lista_espera.getFromId(idPedido.idPedido)===null){
        lista_espera.add({id:idPedido.idPedido,idPedido:idPedido, time:time, cliente:cliente,intentos:1,type:'buscarRepartidor',block:false});
        cliente.emit('respuestaSolicitud',{response:false});
    }
    else{
        if(lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].intentos<max_intentos_lista_espera){
            lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].time = time;
            lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].intentos++;
            lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].block = false;
        }
        else{
            lista_espera.remove({id:lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].id});
            for(var i in web.list){
                web.list[i].emit('lose',{pedidoid:idPedido.idPedido});
            }
        }        
    }
}

function secundero(){    
    if(lista_espera.list.length>0){
        for(var i in lista_espera.list){
            if(typeof(lista_espera.list[i])!=='undefined'){
                if(lista_espera.list[i].time<=0 && !lista_espera.list[i].block){
                    switch(lista_espera.list[i].type){
                        case 'buscarRepartidor': 
                            buscarRepartidor(lista_espera.list[i].cliente,lista_espera.list[i].idPedido);
                            lista_espera.list[i].block = true;
                        break;
                        case 'rankear': 
                            lista_espera.list[i].cliente.emit('rankearRepartidor',{idPedido:lista_espera.list[i].id,repartidores_id:lista_espera.list[i].repartidores_id});
                            lista_espera.remove(lista_espera.list[i]);
                        break;
                        case 'asignarRepartidorOffLine':
                            asignarRepartidorOffLine(lista_espera.list[i].cliente,lista_espera.list[i].idPedido);
                            lista_espera.remove(lista_espera.list[i]);
                        break;
                    }            
                }else{
                    lista_espera.list[i].time--;
                }
            }else{
                lista_espera.remove(lista_espera.list[i]);
            }
        }
    }
    timerSecundero = setTimeout(secundero,1000);
}
secundero();



/** Para el corte diario ***/
var fechas = new Date();
var timeCorte = fechas.getFullYear()+'-'+(parseInt(fechas.getMonth())+1)+'-'+fechas.getDate();

function realTime(){    
    if(web.list.length>0){
        pizzasdb.getDatosPedidos(function(data){
            data.repartidores = repartidores.list.length;
            data.sucursales = [];
            for(var i in clientes.list){
                if(typeof(clientes.list[i])!=='undefined' && typeof(clientes.list[i].id)!=='undefined'){
                    data.sucursales.push({id:clientes.list[i].id});
                }
            }
            data.espera = lista_espera.list.length;
            //Enviar repartidores online
            data.locations = [];
            for(var r in repartidores.list){
                var re = repartidores.list[r];
                data.locations.push({id:re.id,lat:re.lat,lon:re.lon,nombre:re.nombre});
            }
            data.time = timeCorte;
            for(var i in web.list){
                web.list[i].emit('refresh',data);
            }
            timerRealTime = setTimeout(realTime,600);
        },timeCorte);
    }else{
        timerRealTime = setTimeout(realTime,600);
    }
}

realTime();
server.listen(3000);