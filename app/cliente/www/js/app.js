// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    cache:false,
    templateUrl: 'templates/main/tabs.html',
    controller: 'Socket'
  })

  // Each tab has its own nav history stack:

  .state('tab.main', {
    url: '/main',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/main.html',
        controller:'Main'
      }
    }
  })
  
.state('tab.cuenta', {
    cache:false,
    url: '/cuenta',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/cuenta.html',
        controller: 'Cuenta'
      }
    }
  })
  
  .state('tab.balance', {
    cache:false,
    url: '/balance',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/balance.html',
        controller: 'Balance'
      }
    }
  })
  
  .state('tab.producto', {
    cache:false,
    url: '/producto/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/producto.html',
        controller: 'Producto'
      }
    }
  })

  .state('tab.carrito', {
    cache:false,
    url: '/carrito',
    views: {
      'tab-main': {
        templateUrl: 'templates/main/carrito.html',
        controller: 'Carrito'
      }
    }
  })
  
  .state('tab.disconected', {
    url: '/disconected',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/disconnected.html',
        controller:'Main'
      }
    }
  })
  
  .state('tab.direccion', {
    url: '/direccion',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/direccion_entrega.html',
        controller:'Direccion_entrega'
      }
    }
  })
  
 .state('tab.pagar', {
    url: '/pagar',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/pagar.html',
        controller:'Pagar'
      }
    }
  })
  
  .state('tab.reintentarpago', {
    url: '/reintentarpago/:ventaid',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/pagar.html',
        controller:'Reintentarpago'
      }
    }
  })
  
 .state('tab.mapa', {
    url: '/mapa',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/mapa.html',
        controller:'Mapa'
      }
    }
  })
  
 .state('tab.ventas', {
    url: '/ventas',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/ventas.html',
        controller:'Ventas'
      }
    }
  })
  
  .state('tab.ventas_detalles', {
    url: '/ventas/:id',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/ventas_detalles.html',
        controller:'Ventas_detalles'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main');

});
