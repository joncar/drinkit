controllers.controller('Ventas_detalles', function($scope,$state,$http,$stateParams,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.urlServer = urlServer;
    Api.list('ventas',{id:$stateParams.id},$scope,$http,function(data){
        $scope.ventas = data[0];
        $scope.ventas.statusText = $scope.ventas.status==='2'?'Recogiendo': $scope.ventas.status==='3'?'En transito':'Completado';
        $scope.ventas.productos = JSON.parse($scope.ventas.productos);
        $scope.ventas.repartidor = JSON.parse($scope.ventas.repartidor);
        $scope.ventas.repartidor.foto = typeof($scope.ventas.repartidor.foto)!=='undefined' && $scope.ventas.repartidor.foto!=='undefined'?urlServer+'/img/fotos/'+$scope.ventas.repartidor.foto:'img/vacio.png';                    
        console.log($scope.ventas);
    });
});