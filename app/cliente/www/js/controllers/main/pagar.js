controllers.controller('Pagar', function($scope,$ionicPopup,$ionicLoading,Api,UI,User,$http,Api,Querys,Group,ConektaService) {  
    $scope.urlServer = urlServer;
    $scope = User.getData($scope);
    $scope.list = Group.load('carrito');
    $scope.list = $scope.list.list;
    $scope.dir = Group.load('direccion_entrega');
        $scope.dir = $scope.dir.list[0];
        $scope.meses = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $scope.fecha = new Date();
        $scope.anio = $scope.fecha.getFullYear();
        $scope.anios = [];
        for(var i=0;i<10;i++){
            $scope.anios.push($scope.anio+i);
        }
        $scope.tarjeta = {
          tarjeta:$scope.tarjeta === 'null'?'':$scope.tarjeta,
          nombre_tarjeta: $scope.nombre_tarjeta === 'null'?'':$scope.nombre_tarjeta,
          cvc: $scope.cvc === 'null'?'':$scope.cvc,
          mes: $scope.mes === 'null'?'':$scope.mes,
          anio:$scope.anio === 'null'?'':$scope.anio,
          guardar:true
        };    

        $scope.sumar = function(){
            $scope.total = 0;
            for(var i in $scope.list){
                $scope.total+= $scope.list[i].total;
            }
        };
        $scope.sumar();

        $scope.pagar = function(){
            var err = false;
            var msj = '';
            if($scope.tarjeta.tarjeta==='' || isNaN(parseInt($scope.tarjeta.tarjeta)) || $scope.tarjeta.tarjeta.length<16){
                msj+= '<div>El numero de tarjeta introducido no es válido</div>';
                err = true;
            }
            if($scope.tarjeta.nombre_tarjeta==='' || $scope.tarjeta.nombre_tarjeta.length<3){
                msj+= '<div>El nombre de el titular introducido no es válido</div>';
                err = true;
            }
            if($scope.tarjeta.cvc==='' || isNaN(parseInt($scope.tarjeta.cvc)) || $scope.tarjeta.cvc.length!==3){
                msj+= '<div>El código de validación introducido no es válido</div>';
                err = true;
            }
            if($scope.tarjeta.mes==='' || isNaN(parseInt($scope.tarjeta.mes))){
                msj+= '<div>El mes de vencimiento introducido no es válido</div>';
                err = true;
            }
            if($scope.tarjeta.anio==='' || isNaN(parseInt($scope.tarjeta.anio))){
                msj+= '<div>El año de vencimiento introducido no es válido</div>';
                err = true;
            }
            if(err){
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Error de validación',msj);
            }else{
                var success = function(data){
                    //desglosamos carrito
                    var articulos = [];
                    for(var i in $scope.list){
                        articulos.push({
                            productos_id:$scope.list[i].id,
                            cantidad:$scope.list[i].cantidad,
                            precio:$scope.list[i].precio,
                            total:$scope.list[i].total
                        });
                    }
                    //Insertamos pedido
                    $scope.data = {
                      clientes_id:$scope.user,
                      direccion_envio:$scope.dir.direccion,
                      ubicacion:'('+$scope.dir.lat+','+$scope.dir.lon+')',
                      lat:$scope.dir.lat,
                      lon:$scope.dir.lon,
                      precio_envio:0,                  
                      total:$scope.total,
                      token:data.id,
                      guardar:$scope.tarjeta.guardar,
                      tarjeta:$scope.tarjeta.tarjeta,
                      nombre_tarjeta:$scope.tarjeta.nombre_tarjeta,
                      cvc:$scope.tarjeta.cvc,
                      mes_vencimiento:$scope.tarjeta.mes,
                      ano_vencimiento:$scope.tarjeta.anio,
                      articulos:JSON.stringify(articulos)
                    };
                    $scope.loading = UI.getLoadingBox($ionicLoading,'Cargando el pedido, por favor espere');
                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                    Api.insert('ventas',$scope,$http,function(data){
                        if(typeof(data.error)!=='undefined'){
                            $scope.showAlert(data.message);
                        }else{
                            $scope.showAlert('Su pago ha sido realizado, gracias por su compra. Le informaremos cuando su pedido este en camino');
                            Group.erase('carrito');
                            Group.erase('direccion_entrega');
                            if($scope.tarjeta.guardar){
                                localStorage.tarjeta = $scope.tarjeta.tarjeta;
                                localStorage.nombre_tarjeta = $scope.tarjeta.nombre_tarjeta;
                                localStorage.cvc = $scope.tarjeta.cvc;            
                                localStorage.mes = $scope.tarjeta.mes;
                                localStorage.anio = $scope.tarjeta.anio;
                            }
                            document.location.href="#/tab/main";
                        }
                    });
                };
                var error = function(data){
                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                    $scope.showAlert('Error de validación',data.message_to_purchaser);
                };
                ConektaService.tokenizar($scope,$scope.tarjeta,success,error);
            }
        };
});