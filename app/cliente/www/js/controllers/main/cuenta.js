controllers.controller('Cuenta', function($rootScope,$scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);       
    $scope.urlServer = urlServer;
    $scope.tarjetin = '***********'+$scope.tarjeta.substring(12);
    $scope.datos = {
        id:$scope.user,
        email:$scope.email,
        password:$scope.password,        
        telefono:$scope.telefono,
        celular:$scope.celular,
        clientes_nombre:$scope.nombre,
        nombre_tarjeta:$scope.nombre_tarjeta!=='undefined'?$scope.nombre_tarjeta:'',
        cvc:$scope.cvc!=='undefined'?$scope.cvc:'',
        mes: $scope.mes === 'null'?'':$scope.mes,
        anio:$scope.anio === 'null'?'':$scope.anio, 
        foto:$scope.foto
    };
    
    $scope.field_types = {
        password:'password'
    };
    
    $scope.send = function(datos){
        if(datos.tarjeta===''){
            datos.tarjeta = $scope.tarjeta;
        }
        $scope.data = datos;        
        Api.update('clientes',$scope.user,$scope,$http,function(data){
            if(data.success){                    
                $scope.data.id = $scope.user;
                User.setData($scope.data);
                if(Api.datos!==null && Api.datos!==undefined){
                    if($scope.data.nombre_tarjeta!=='undefined' && $scope.data.tarjeta!=='undefined' && $scope.data.cvc!=='undefined'){
                        Api.triggerPay = true;
                        //$state.go('tab.aceptarprecio');
                    }else{
                        alert('Para proseguir con el envio debe llenar los datos bancarios');
                    }
                }else{
                    $scope.showAlert = UI.getShowAlert($ionicPopup);
                    $scope.showAlert('Sus datos','Sus datos han sido actualizados con éxito');
                }
            }
        });
    };
    
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
    
    $scope.triggerFile = function(){        
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: 'Elige una opción para seleccionar la imagén',
          title: 'Dispositivo Multimedia',
          subTitle: 'Elige una opción',
          scope: $scope,
          buttons: [
            {
              text: '<b>Camara</b>',              
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.CAMERA);
                  myPopup.close();
              }
            },
            {
              text: '<b>Galería</b>',
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.SAVEDPHOTOALBUM);
                  myPopup.close();
              }
            }
          ]
        });
        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });
    };
    
    $scope.triggerDevice = function(sourceType){
        navigator.camera.getPicture(function(imageURI){
            $scope.selectImage(imageURI);            
        },
        function(message) { $scope.showAlert('get picture failed'); },
        {
            quality: 50, 
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: sourceType,            
            targetWidth: 284, 
            targetHeight: 284,
            allowEdit: true
        }
        );
    };
    
    $scope.selectImage = function(imageURI){
        $scope.data = {clientes_id:$scope.user, foto:imageURI};
        Api.query('uploadPhotoCliente',$scope,$http,function(data){
            $scope.showAlert('Foto subida','Su foto ha sido guardada con éxito');
            $scope.datos.foto = data.trim();
            localStorage.foto = data.trim();
            $rootScope.$broadcast('reload');
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });
    }; 
});

controllers.controller('Balance', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');    
    $scope.data = {'clientes_id':$scope.user};
    $scope.detail = {envios_hoy:0,envios_mes:0,costo:0,cobranza:0};
    Api.query('getBalance',$scope,$http,function(data){
        //data = data.replace('[]','');
        /*data = JSON.parse(data);
        $scope.detail = data;
        $scope.detail.envios_hoy = parseInt($scope.detail.pedidos_hoy)-parseInt($scope.detail.solicitudes_hoy);
        $scope.detail.envios_mes = parseInt($scope.detail.pedidos_mes)-parseInt($scope.detail.solicitudes_mes);
        console.log(data);*/
    });
});