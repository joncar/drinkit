controllers.controller('Carrito', function($scope,$ionicModal,Api,UI,User,$http,Api,Querys,Group) {  
    $scope = User.getData($scope);
    $scope.urlServer = urlServer;
    $scope.car = {};
    $scope.refreshCart = function(params){
        $scope.car.id = params.id;
        $scope.car.nombre = params.nombre;
        $scope.car.foto_portada = params.foto_portada;
        $scope.car.cantidad = params.cantidad;
        $scope.car.precio = parseFloat(params.precio);
        $scope.car.total = $scope.car.precio*$scope.car.cantidad;
        $scope.car.composicion = params.composicion;
    };

    $scope.addCartModal = function(params){
        $scope.refreshCart(params);
        UI.getModalBox($ionicModal,'templates/modals/cart.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };

    $scope.addCart = function(param){
      $scope.closeModal();
      param.cantidad = param.cantidad || 1;
      var carro = Group.load('carrito');
      carro.add(param);
      carro.save('carrito');
      $scope.list = carro.list;
      $scope.sumar();
    };
    
    $scope.sumar = function(){
        $scope.total = 0;
        for(var i in $scope.list){
            $scope.total+= $scope.list[i].total;
        }
    };

    $scope.remCart = function(id){
        var carro = Group.load('carrito');
        carro.remove({id:id});
        carro.save('carrito');
        $scope.list = carro.list;
        $scope.sumar();
    };

    $scope.procesar = function(){
            document.location.href="#/tab/direccion";            
    };
    
    $scope.carrito = Group.load('carrito');
    $scope.list = $scope.carrito.list;
    $scope.sumar();
});