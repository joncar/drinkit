controllers.controller('Socket', function($rootScope,$scope,$ionicSideMenuDelegate,$ionicPlatform,User,$http,Api,pushGcm) {
    $scope = User.getData($scope);
    $scope.foto = typeof($scope.foto)!=='undefined' && $scope.foto!=='undefined'?urlServer+'/img/clientes/'+$scope.foto:'img/vacio.png';
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
        navigator.app.exitApp();
    };
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type === Connection.NONE) {
                $scope.showAlert('¿Haz perdido la conexión a internet?, necesitas conectarte para poder seguir usando la aplicación');
            }
        }        
        pushGcm.init($scope);
    });
    
    $rootScope.$on('reload',function(){
        $scope = User.getData($scope);
        $scope.foto = typeof($scope.foto)!=='undefined' && $scope.foto!=='undefined'?urlServer+'/img/clientes/'+$scope.foto:'img/vacio.png';
    });
});