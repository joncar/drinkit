var myPopup = [];
var controllers = angular.module('starter.controllers',['ionic']);
localStorage.innerHeight = window.innerHeight+'px';
controllers.controller('Main', function($scope,$ionicModal,$ionicViewService,$stateParams,$http,Querys,Api,UI,User,Api,Group,Querys) {  
    $scope.urlServer = urlServer;
    $scope.search = {};
    $scope.loadingSpinner = true;    
    $ionicViewService.clearHistory();
    Api.list('categorias',{},$scope,$http,function(data){
        $scope.categorias = data;
        Api.categorias = data;
        if(data.length>0){
            $scope.activar(0);
        }            
    });
    $scope.activar = function(id){
        $scope.loadingSpinner = true;
        for(var i in $scope.categorias){
            $scope.categorias[i].active = false;
        }
        $scope.categorias[id].active = true;
        $scope.search['productos.categorias_id'] = $scope.categorias[id].id;
        $scope.buscar($scope.search);
    };
    $scope.buscar = function(valores){
        $scope.loadingSpinner = true;
        $scope.$broadcast('scroll.refreshComplete');
        if(typeof($scope.closeModal)!=='undefined'){
            $scope.closeModal();
        }
        Querys.productos($scope,valores,function(data){
            $scope.productos = data;
            $scope.loadingSpinner = false;            
        });
    };
    $scope.searchModal = function(){
        UI.getModalBox($ionicModal,'templates/modals/search.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
    if(typeof($stateParams.filtro)!=='undefined' && typeof($stateParams.filtroid)!=='undefined'){
        $scope.search['productos.'+$stateParams.filtro] = $stateParams.filtroid;
    }    
    $scope.car = {};
    $scope.refreshCart = function(params){
        $scope.car.id = params.id;
        $scope.car.nombre = params.productos_nombre || $scope.car.nombre;
        $scope.car.foto_portada = params.foto_portada;
        $scope.car.cantidad = params.cantidad;
        $scope.car.cant = $scope.car.cantidad || 1;
        $scope.car.precio = parseFloat(params.precio);
        $scope.car.total = $scope.car.precio*$scope.car.cant;
        $scope.car.composicion = params.composicion;
    };
    $scope.addCartModal = function(params){
        $scope.refreshCart(params);
        UI.getModalBox($ionicModal,'templates/modals/cart.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
    $scope.addCart = function(param){
      $scope.closeModal();
      param.cantidad = param.cantidad || 1;
      var carro = Group.load('carrito');
      carro.add(param);
      carro.save('carrito');
    };
    
    $scope.addCartAndPay = function(param){
        $scope.addCart(param);
        document.location.href="#tab/carrito";
    };
    $scope.getCant = function(index){
        var carro = Group.load('carrito');
        var item = carro.getFromId(index);
        return item===null?0:item.cantidad;
    }
});