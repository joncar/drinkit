controllers.controller('Direccion_entrega', function($scope,User,Querys,Api,Group,getDirFromGPS) {  
    $scope.urlServer = urlServer;
    $scope = User.getData($scope);
    $scope.list = Group.load('carrito');
    $scope.list = $scope.list.list;
    Api.innerHeight = window.innerHeight;
    Querys.ajustes($scope,function(data){
        $scope.ajustes = data[0];
    });
    
    $scope.sumar = function(){
        $scope.total = 0;
        for(var i in $scope.list){
            $scope.total+= $scope.list[i].total;
        }
    };    
    $scope.sumar();
    $scope.dir = {direccion:''};
    $scope.showFields = false;
    $scope.selectFieldsFunction = function(place){
        var direccion = $scope.dir.direccion;
        $scope.dir = getDirFromGPS.selectFieldsFunction(place);   
        $scope.dir.direccion = $scope.dir.direccion===undefined?direccion:$scope.dir.direccion;
        $scope.dir.lat = place.lat;
        $scope.dir.lon = place.lon;
        $scope.showFields = true;
        if(!$scope.$$phase){
            $scope.$apply('dir');
        }
    };
    
    $scope.gps = function(){
      getDirFromGPS.getDirFromGps(function(lat,lon,dir){
          $scope.dir.direccion = dir.direccion;
          dir.lat = lat;
          dir.lon = lon;
          $scope.selectFieldsFunction(dir);
      });
    };
    
   $scope.procesar = function(){
        if($scope.dir.calle===undefined || $scope.dir.ext===undefined || $scope.dir.colonia===undefined || $scope.dir.delegacion===undefined || $scope.dir.ciudad===undefined){
            alert('Por favor complete los datos requeridos, para poder llevar sus producto');
        }else{
            
            var d = Group;
            d.list = [];
            $scope.dir.id = 1;
            d.erase('direccion_entrega');
            d.add($scope.dir);            
            if(typeof($scope.dir.lat)!=='undefined' && $scope.dir.lon!=='undefined'){
                d.list[0].direccion = getDirFromGPS.mergeDirection(d.list[0]);
                d.save('direccion_entrega');
                document.location.href="#/tab/pagar";
            }else{
                d.save('direccion_entrega');
                document.location.href="#/tab/mapa";
            }
        }
   };
});