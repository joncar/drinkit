controllers.controller('Producto', function($scope,$stateParams,$ionicModal,Api,UI,User,$http,Api,Querys) {  
    $scope.urlServer = urlServer;        
    Querys.categorias($scope,function(data){
        $scope.categorias = data;
    });    
    
    $scope.buscar = function(valores){
        $scope.loadingSpinner = true;        
        if(typeof($scope.closeModal)!=='undefined'){
            $scope.closeModal();
        }
        Querys.productos($scope,valores,function(data){
            $scope.producto = data[0];
            $scope.refreshCart(data[0]);
            $scope.loadingSpinner = false;
        });
    };
    $scope.addCartModal = function(){
        UI.getModalBox($ionicModal,'templates/modals/cart.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
    $scope.addCart = function(param){
      $scope.closeModal();
      alert('Producto añadido al carrito');
    };
    $scope.car = {};
    $scope.refreshCart = function(params){
        $scope.car.nombre = params.productos_nombre || $scope.car.nombre;
        $scope.car.cantidad = params.cantidad || 1;
        $scope.car.precio = parseFloat(params.precio);
        $scope.car.total = $scope.car.precio*$scope.car.cantidad;
    };
    $scope.search = {};
    if(typeof($stateParams.id)!=='undefined'){
        $scope.search['productos.id'] = $stateParams.id;
    }
    $scope.buscar($scope.search);
});