controllers.controller('Mapa', function($scope,getDirFromGPS,Api,User,Group) {
    $scope = User.getData($scope);    
    if(typeof(localStorage.lat)==='undefined'){
        localStorage.lat = '47.55633987116614';
        localStorage.lon = '7.576619513223015';
    }
    $scope.basel = {lat:localStorage.lat,lon:localStorage.lon};
    
    $scope.innerheight = localStorage.innerHeight;
    $scope.iconMap = 'http://74.208.77.223/img/fooddeliveryservice.png';
    $scope.dir = Group.load('direccion_entrega');
    if(typeof($scope.dir)!=='undefined' && $scope.dir.list.length>0){
        $scope.direccion = $scope.dir.list[0].direccion;

        $scope.location = function(value){
            console.log(value);
            $scope.basel = {lat:value.lat,lon:value.lon};        
        };

        $scope.cancelar = function(){
          document.location.href="#/tab/direccion";  
        };

        $scope.confirmar = function(){           
          var d = $scope.dir.list[0];
          d.lat = $scope.basel.lat;
          d.lon = $scope.basel.lon;
          d.direccion = getDirFromGPS.mergeDirection(d);
          Group.add(d);
          Group.save('direccion_entrega');
          document.location.href="#/tab/pagar";      
        };

        $scope.gotoLocation = function (lat, lon) {            
            if ($scope.lat !== lat || $scope.lon !== lon) {
               $scope.basel = { lat: lat, lon: lon };
            }
        };
    }else{
        document.location.href="main.html#/tab/main";
    }
});