controllers.controller('Productos', function($scope,$stateParams,$ionicModal,Api,UI,User,$http,Api,Querys) {  
    $scope.urlServer = urlServer;        
    Querys.categorias($scope,function(data){
        $scope.categorias = data;
    });
    
    $scope.buscar = function(valores){
        $scope.loadingSpinner = true;
        $scope.$broadcast('scroll.refreshComplete');
        if(typeof($scope.closeModal)!=='undefined'){
            $scope.closeModal();
        }
        Querys.productos($scope,valores,function(data){
            $scope.productos = data;
            $scope.loadingSpinner = false;            
        });
    };
    $scope.searchModal = function(){
        UI.getModalBox($ionicModal,'templates/modals/search.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
    $scope.search = {};
    if(typeof($stateParams.filtro)!=='undefined' && typeof($stateParams.filtroid)!=='undefined'){
        $scope.search['productos.'+$stateParams.filtro] = $stateParams.filtroid;
    }
    $scope.buscar($scope.search);
    
    
});