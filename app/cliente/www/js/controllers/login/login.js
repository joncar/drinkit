angular.module('starter.controllers', ['ngOpenFB'])

.controller('Login', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);       
       $scope.validEnter = function(){
            if(localStorage.email!==undefined){                     
               $scope = User.getData($scope);
               document.location.href="main.html";
            }
       };

       $scope.login = function(param){
           if(param!==undefined){
                $scope.email = param.email;
                $scope.password = param.password;
                $scope.isRegister();
            }
       };

       $scope.isRegister = function(){
           data =  {email:$scope.email,password:$scope.password};            
           Api.list('clientes',data,$scope,$http,function(data){
                 if(data.length==0){                   
                    $scope.showAlert('Inicio de sesión','Email o Contraseña Incorrecta');
                 }
                 else{
                       data = data[0];
                       User.setData(data);
                       document.location.href="main.html";
                 }
           },'where');         
       }
       $scope.validEnter();
})

.controller('Registrar', function($scope,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,User,Api,UI) {
            $scope.loading = UI.getLoadingBox($ionicLoading);       
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.modal = UI.getModalBox($ionicModal,'templates/modals/condiciones.html',$scope);
            $scope.data = {};            
            $scope.personales = function(data){
              if(data!=undefined){
                 if(data.ciudad!=undefined && data.condiciones){
                    data.direccion = data.ciudad+' '+data.calle;
                    Api.data = data;
                    Api.data.condiciones = null;
                    $scope.data = Api.data;
                    Api.insert('clientes',$scope,$http,function(data){
                      if(data.success){
                          $scope.showAlert('Sus datos han sido guardados');
                          $scope.data.id = data.insert_primary_key;
                          User.setData($scope.data);
                          document.location.href="main.html";
                      }
                    });
                 }
                 else{
                     $scope.showAlert('Debe indicar sus datos personales para continuar');
                 }
              }  
              else{
                    $scope.showAlert('Debe indicar sus datos personales para continuar');
              }
            };                           
            $scope.mostrarCondiciones = function(data){
                 if(data){
                     $scope.toggleModal('show');
                 }
             };
})

.controller('Recover', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);
       
       $scope.login = function(data){
            $scope.data = {table:'clientes',email:data.email};
            Api.query('recover',$scope,$http,function(data){
                alert('Su contraseña ha sido reseteada, por favor verifique su correo para continuar con el proceso de activación');
                document.location.href="index.html";
            });
       };
});
