services.factory('ConektaService', function($http,Api,Querys) {
    return {      
      getToken:function(datos,callbackSuccess,callbackFail){
          Conekta.setPublishableKey(Api.ajustes.public_key);
          $form = {
            card:{
                'name':datos.nombre_tarjeta,
                'number':datos.tarjeta,
                'cvc':datos.cvc,
                'exp_month':datos.mes,
                'exp_year':datos.anio
            }
        };
        Conekta.token.create($form,
        function(data){
            callbackSuccess(data);
        }, 
        function(data){
            callbackFail(data);
        });
      },
      tokenizar:function($scope,datos,callbackSuccess,callbackFail){          
            var instance = this;
            if(Api.ajustes===undefined){
                Querys.ajustes($scope,function(data){
                    instance.getToken(datos,callbackSuccess,callbackFail);
                });
            }else{
                instance.getToken(datos,callbackSuccess,callbackFail);
            }
      }
    };
});