//var url = 'http://localhost/proyectos/pizzas/api/';
//var ipServer = 'http://localhost:3001'; //Ip del servidor socket
var url = 'http://74.208.77.223/api/';
var ipServer = 'http://74.208.77.223:3000'; //Ip del servidor socket
var myPopup;
var ws;
var userconnect = localStorage.user;
var services = angular.module('starter.services', [])
.factory('Api', function() {
  var email = '';
  var password = '';
  var data = "";
  return {    
    list:function(controller,data,$scope,$http,successFunction,operator){
        if(typeof(data)=='object'){
            var s = '';            
            for(var i in data){
                s+= 'search_field[]='+i+'&search_text[]='+data[i]+'&';
            }
            data = s;
            data += operator==undefined?'operator=where':'operator='+operator;
        }
        if($scope.loading!=undefined){
            $scope.loading('show');
        }
        $http({
            url:url+controller+'/json_list',
            method: "POST",
            data:data,
            transformRequest:angular.identity,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
              'ver': version,
              'cliente':userconnect
            }           
          }).success(function(data){
             if(data==='denied'){
              alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
             }
             if(successFunction!==undefined){
                 successFunction(data);
                if($scope.loading!=undefined){
                    $scope.loading('hide');
                }
             }
          })
        .catch(function(data, status, headers, config){ // <--- catch instead error            
            if($scope.loading!=undefined){
                $scope.loading('hide');
            }
            alert('Ha sido imposible conectar con el servidor, por favor verifica tu conexión a internet');
        });
    },
    insert:function(controller,$scope,$http,successFunction){       
       if(!$scope.data){//Si ls datos son de un formulario
            d = document.getElementById('formreg');
            data = new FormData(d);  
        }//Si se envia el array
        else{
            data = new FormData();
            for(i in $scope.data){
              data.append(i,$scope.data[i]);
            }            
        }
       if(typeof($scope.loading)!=='undefined'){
            $scope.loading('show');
        }     
       $http({
            url:url+controller+'/insert_validation',
            method: "POST",
            data:data,
            transformRequest: angular.identity,
            headers: {
              'Content-Type':undefined,
              'ver': version,
              'cliente':userconnect
            }           
          }).success(function(data){
              data = data.replace('<textarea>','');
              data = data.replace('</textarea>','');
              data = JSON.parse(data);
              if(data.success){
                  if(!$scope.data){//Si ls datos son de un formulario
                        d = document.getElementById('formreg');
                        data = new FormData(d);  
                  }//Si se envia el array
                  else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }            
                  }       
                  $http({
                        url:url+controller+'/insert',
                        method: "POST",
                        data:data,
                        transformRequest: angular.identity,
                        headers: {
                          'Content-Type':undefined,
                          'ver': version,
                          'cliente':userconnect
                        }           
                      }).success(function(data){
                          if(data==='denied'){
                            alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
                          }
                          data = data.replace('<textarea>','');
                          data = data.replace('</textarea>','');
                          data = JSON.parse(data);                          
                          if(data.success){
                               if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                               }
                          }
                          else{
                             if(typeof($scope.loading)!=='undefined'){
                                $scope.loading('hide');
                            }
                             $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                          }
                        })
                        .error(function(){
                            alert('Ha ocurrido un error interno, contacte con un administrador');
                        });
              }
              else{
                  if(typeof($scope.loading)!=='undefined'){
                        $scope.loading('hide');
                    }
                  $scope.showAlert('Ocurrio un error al añadir',data.error_message);
              }
          })
          .error(function(data){
              alert('Ha ocurrido un error interno, contacte con un administrador');
          });
        },
        
        update:function(controller,id,$scope,$http,successFunction){
        if(!$scope.data){//Si ls datos son de un formulario
          d = document.getElementById('formreg');
          data = new FormData(d);
        }//Si se envia el array
        else{
          data = new FormData();
          for(i in $scope.data){
            data.append(i,$scope.data[i]);
          }
        }
        if(typeof($scope.loading)!=='undefined'){
            $scope.loading('show');
        }          
        $http({
             url:url+controller+'/update_validation/'+id,
             method: "POST",
             data:data,
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined,
               'ver': version,
               'cliente':userconnect
             }           
           }).success(function(data){
               data = data.replace('<textarea>','');
               data = data.replace('</textarea>','');
               data = JSON.parse(data);
               if(data==='denied'){
                alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
               }
               if(data.success){
                    if(!$scope.data){//Si ls datos son de un formulario
                      d = document.getElementById('formreg');
                      data = new FormData(d);
                    }//Si se envia el array
                    else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }
                    }       
                   $http({
                         url:url+controller+'/update/'+id,
                         method: "POST",
                         data:data,
                         transformRequest: angular.identity,
                         headers: {
                           'Content-Type':undefined,
                           'ver': version,
                           'cliente':userconnect
                         }           
                       }).success(function(data){
                           data = data.replace('<textarea>','');
                           data = data.replace('</textarea>','');
                           data = JSON.parse(data);                          
                           if(data.success){
                                if(successFunction!==undefined){
                                   successFunction(data);
                                   if(typeof($scope.loading)!=='undefined'){
                                        $scope.loading('hide');
                                    }
                                }
                           }
                           else{
                              if(typeof($scope.loading)!=='undefined'){
                                    $scope.loading('hide');
                                }
                              $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                           }
                    });
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al añadir',data.error_message);
               }
          });
        },
        
        deleterow:function(controller,id,$scope,$http,successFunction){        
        $scope.loading('show');
        $http({
             url:url+controller+'/delete/'+id,
             method: "GET",
             data:'',
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined,
               'ver': version,
               'cliente':userconnect
             }           
           }).success(function(data){
               if(data==='denied'){
                alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
               }
               if(data.success){
                    if(successFunction!==undefined){
                        successFunction(data);
                        $scope.loading('hide');
                     }
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al eliminar',data.error_message);
               }
          });
        },

        initInterface:function($scope){

          return $scope;
        },

        query:function(controller,$scope,$http,successFunction){
            if(!$scope.data){//Si ls datos son de un formulario
              d = document.getElementById('formreg');
              data = new FormData(d);
            }
            else{
                data = new FormData();
                for(i in $scope.data){
                  data.append(i,$scope.data[i]);
                }
            }            
            
            if($scope.loading!=undefined){
                $scope.loading('show');
            }
            $http({
                 url:url+controller,
                 method: "POST",
                 data:data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':undefined,
                   'ver': version,
                   'cliente':userconnect
                 }           
               }).success(function(data){
                  if(data==='denied'){
                    alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
                   }
                    if(successFunction!==undefined){
                        successFunction(data);
                        if($scope.loading!=undefined){
                            $scope.loading('hide');
                        }
                     }
              }).error(function(data){
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al enviar los datos',data);
              });
        }
    }   
})

.factory('UI', function() {
    return {    
        showPopupBox:function($ionicPopup,$scope,message,title,buttonAccept){
            $scope.data = {}
            // An elaborate, custom popup
              myPopup = $ionicPopup.show({
              template: message,
              title: title,              
              scope: $scope,
              buttons: [
                { 
                 text: 'Cancelar',
                 onTap:function(e){
                     myPopup.close();
                 }                 
                },
                buttonAccept
              ]
            });
            myPopup.then(function(res) {
              console.log('Tapped!', res);
            });
            
            /*ButtonAccept = {text: buttonAccept['text'],
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.wifi) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                return $scope.data.wifi;
              }
            }
          }}*/
        },
        getShowAlert:function($ionicPopup){            
            return function(title,template){
                if(this.alertMostrado===undefined){                                    
                    var scope = this;
                    this.alertMostrado = $ionicPopup.show({
                    template: template,
                    title: title,                
                    buttons: [                  
                      {
                        text: '<b>Aceptar</b>',
                        type: 'button-positive',
                        onTap: function(e) {                      
                          scope.alertMostrado.close();
                          scope.alertMostrado = undefined;
                        }
                      }
                    ]
                  });
                };
            };
        },
        getLoadingBox:function($ionicLoading,message){
            message = message==undefined?'Cargando...':message;
            return function(attr){            
                attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: message});
            };
        }, 
        
        getConfirmBox:function($ionicPopup) {
           return function(title,template,acceptFunction,denyFunction){
               var confirmPopup = $ionicPopup.confirm({
                    title: title,
                    template: template
                  });
                  confirmPopup.then(function(res) {
                    if(res) {
                      if(acceptFunction)acceptFunction();
                    } else {
                      if(denyFunction)denyFunction();
                    }
                  });
            };
        },
        
        getModalBox:function($ionicModal,template,$scope,callback){
            $ionicModal.fromTemplateUrl(template, {//Invitar personas
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
                if(typeof(callback)!=='undefined'){
                    callback(modal);
                }
              });

            $scope.toggleModal = function(attr) {
              if(attr==undefined || attr=='show'){
                  $scope.modal.show();
              }
              else{
                  $scope.modal.hide();    
              }
            };            
            
            $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {                
                $scope.toggleModal('hide');
            });
        },
        
        getLocationName:function(lat,lon,callback){
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'latLng': new google.maps.LatLng(lat,lon)}, function(results, status) {
                //calle                                 
                callback(results[0]);
            });
        }
    };
})


.factory('SocketConnection', function() {  
  return {
        ipServer:ipServer, //Ip del servidor socket        
        WebSocketOpen:function($scope){
            if(typeof(io)!=='undefined'){
                ws = io.connect(this.ipServer,{'force new connection': true});
                ws.on('connect',function(){
                    $scope.connected();
                    ws.on('onopen', function() {
                       if(ws.userID!=undefined){
                           ws.connectID('connectID',{id:ws.userID});
                       }
                       console.log('Conexion con el socket establecida');
                    });
                });

                ws.on('disconnect',function(){
                    $scope.disconnected();
                });
            }
            //Eventos personalizados, Borrar a la hora de reutilizar en otro proyecto            
        },
        
        connectID:function(userID){ //Borrar solo si es necesario Esta función envia el ID del usuario al servidor
            if(typeof(ws)!=='undefined'){
                ws.connectID = function(evt,datos){
                    datos.type = 'sucursal';
                    ws.emit(evt,datos);
                }
                ws.connectID('connectID',{id:userID});
                ws.userID = userID;
            }
        },
        
        buscarRepartidor:function(idPedido,callback){
            ws.emit('buscarRepartidor',{idPedido:idPedido});
        }, 
        
        programar:function(idPedido,time,callback){
            ws.emit('programar',{idPedido:idPedido,time:time});
        }
    };
})

.factory('User', function() {  
  return {
        setData:function(data){            
            localStorage.user = data.id;
            localStorage.nombre = data.clientes_nombre;
            localStorage.email = data.email;
            localStorage.telefono = data.telefono;  
            localStorage.celular = data.celular;
            localStorage.password = data.password;
            localStorage.tarjeta = data.tarjeta;
            localStorage.nombre_tarjeta = data.nombre_tarjeta;
            localStorage.cvc = data.cvc;            
            localStorage.mes = data.mes_vencimiento;
            localStorage.anio = data.ano_vencimiento;
            localStorage.foto = data.foto;
        },
        
        getData:function($scope){
            $scope.user = localStorage.user;
            $scope.email = localStorage.email;        
            $scope.nombre = localStorage.nombre;                  
            $scope.telefono = localStorage.telefono;                  
            $scope.celular = localStorage.celular;            
            $scope.password = localStorage.password;
            $scope.tarjeta = localStorage.tarjeta;
            $scope.nombre_tarjeta = localStorage.nombre_tarjeta;
            $scope.cvc = localStorage.cvc;
            $scope.fecha_vencimiento = localStorage.fecha_vencimiento;
            $scope.private_key = localStorage.private_key;
            $scope.public_key = localStorage.public_key;
            $scope.mes = localStorage.mes;
            $scope.anio = localStorage.anio;
            $scope.foto = localStorage.foto;
            return $scope;
        },
        
        cleanData:function(){
            localStorage.removeItem('user');
            localStorage.removeItem('nombre');            
            localStorage.removeItem('email');            
            localStorage.removeItem('telefono');            
            localStorage.removeItem('celular');                        
            localStorage.removeItem('password');            
            localStorage.removeItem('tarjeta');
            localStorage.removeItem('nombre_tarjeta');
            localStorage.removeItem('cvc');
            localStorage.removeItem('fecha_vencimiento');            
            localStorage.removeItem('gcm');
            localStorage.removeItem('mes');
            localStorage.removeItem('anio');
            localStorage.removeItem('foto');
        }
    }  
})

.factory('Group', function() {  
  return {
     list:[],    
     add:function(cliente){
        var pos = this.getPosition(cliente.id);
        if(pos===null)
        this.list.push(cliente);
        else this.list[pos] = cliente;
    },    
    remove:function(cliente){
        if(cliente!==undefined){
            var aux = [];
            for(var i in this.list){
                if(this.list[i].id!==cliente.id){
                    aux.push(this.list[i]);
                }
            }
            this.list = aux;
        }
    },
    update:function(cliente){
        for(var i in this.list){
            if(this.list[i].id===cliente.id){
                this.list[i] = cliente;
            }
        }
    },
    getFromId:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = this.list[i];
            }
        }
        return cliente;
    },    
    getPosition:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = i;
            }
        }
        return cliente;
    },
    save:function(name){
        this.erase(name);
        localStorage[name] = JSON.stringify(this.list);
    },
    load:function(name){
        var l = typeof(localStorage[name])!=='undefined'?JSON.parse(localStorage[name]):[];
        this.list = typeof(this.list)!=='undefined'?l:[];
        if(this.list===undefined){
            this.list = [];
        }
        return this;
    },
    erase:function(name){
        localStorage.removeItem(name);
    }
  };
});