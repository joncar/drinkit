services.factory('Querys', function($http,Api) {
    return {
      categorias:function($scope,callback){
          if(typeof(Api.categorias)==='undefined'){
                Api.list('categorias',{},$scope,$http,function(data){
                      Api.categorias = data;
                      callback(data);
                });
          }else{
              callback(Api.categorias);
          }
      },
      productos:function($scope,query,callback){
          Api.list('productos',query,$scope,$http,function(data){
                for(var i in data){
                    data[i].categorias = JSON.parse(data[i].sd5f8a66b);                    
                }
                callback(data);
            });
      },
      ajustes:function($scope,callback){
          if(Api.ajustes===undefined){
                Api.list('ajustes',{},$scope,$http,function(data){
                      for(var i in data){
                          Api.ajustes = data[0];
                      }
                      callback(data);
               });
        }
      }
    };
});