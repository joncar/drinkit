MapApp.directive('placesAutocomplete',function($window){
    return {
        restrict:"AE",
        replace:true,
        require: 'ngModel',
        template:'<input type="text" class="autocompletegoogleplace" id="autocompletegoogleplace" style="width:100%">',
        scope:{
            selectfields:'=',
            indice:'=',
            tipo:'=',
        },
        link:function(scope,element,attrs,model){
            var input = element[0];
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);                        
            function fillInAddress() {
                // Get the place details from the autocomplete object.
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
                
                var place = autocomplete.getPlace();               
                scope.selectfields(place,scope.indice,scope.tipo);
              }
              element.bind('focus',function(){
                    //Disable Tap
                   var container = document.getElementsByClassName('pac-container');
                   angular.element(container).attr('data-tap-disabled', 'true');
                   angular.element(container).css('pointer-events', 'auto');
                   angular.element(container).on("click", function(){
                       var e = document.getElementsByClassName('autocompletegoogleplace').length;
                       for(var i=0;i<e;i++){
                           document.getElementsByClassName('autocompletegoogleplace')[i].blur();
                       }
                   }); 
              });              
        }
    };
});

MapApp.directive("appMap", function ($window) {
    return {
        restrict: "E",
        replace: true,        
        template: "<div data-tap-disabled='true'></div>",
        scope: {
            center: "=",        // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
            markers: "=",       // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
            map:'=',
            direccion:'=',
            location:'=',
            icon:'=',
            lugares:'=',
            widthmap: "=",         // Map width in pixels.
            heightmap: "=",        // Map height in pixels.
            zoom: "@",          // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
            mapTypeId: "@",     // Type of tile to show on the map (roadmap, satellite, hybrid, terrain).
            panControl: "@",    // Whether to show a pan control on the map.
            zoomControl: "@",   // Whether to show a zoom control on the map.
            scaleControl: "@"   // Whether to show scale control on the map.
        },
        link: function (scope, element, attrs,ngModelCtrl) {
            var toResize, toCenter;
            var map;
            var infowindow;
            var currentMarkers;
            var callbackName = 'InitMapCb';
            var positionMark = null;
            var directionsService;
            var directionsDisplay;
            var marksdrawedspoints = [];
            var el = element[0];
            el.style.width = scope.widthmap===undefined?window.innerWidth+'px':scope.widthmap;
            el.style.height = scope.heightmap===undefined?window.innerHeight-50+'px':scope.heightmap;
            // callback when google maps is loaded            
            function createMap() {
                    console.log("map: create map start");
                    var mapOptions = {
                                zoom: parseInt(scope.zoom),
                                center: new google.maps.LatLng(scope.center.lat,scope.center.lon),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                panControl: true,
                                zoomControl: true,
                                mapTypeControl: true,
                                scaleControl: false,
                                streetViewControl: false,
                                navigationControl: true,
                                disableDefaultUI: true,
                                overviewMapControl: true
                    };
                    
                    if (!(map instanceof google.maps.Map)) {
                            map = new google.maps.Map(el, mapOptions);                            
                            directionsService = new google.maps.DirectionsService;
                            directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                            directionsDisplay.setMap(map);
                            if(scope.direccion!==null && scope.direccion!==undefined && scope.direccion!==' '){                                
                                searchDireccion(scope.direccion);
                            }
                            //google.maps.event.addDomListener(positionMark, 'mouseup', function(e) {
                            positionMark = new google.maps.Marker({ draggable:true, position: new google.maps.LatLng(scope.center.lat,scope.center.lon), map: map, title: 'Mi posición',icon:scope.icon });
                            google.maps.event.addDomListener(positionMark, 'dragend', function(e) {
                                    scope.location({lat:e.latLng.lat(),lon:e.latLng.lng()});
                            });
                     }
             }
             
            createMap();
            updateMarkers();

            scope.$watch('center', function() {                    
                    updateMarkers();                    
            });
            
            scope.$watch('markers', function() {                    
                    drawMarkers();
            });
            
            scope.$watch('direccion', function() {                    
                    if(scope.direccion!==null && scope.direccion!==undefined && scope.direccion!==' '){                                
                        searchDireccion(scope.direccion);
                    }
            });
            
            function searchDireccion(direccion){
                if (map) {                    
                    var geocoder = new google.maps.Geocoder();
                    var address = direccion;
                    geocoder.geocode({'address': address}, function(results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                for(var i in results){                                    
                                    CrearMarcas(results[i]);
                                }
                                if(scope.lugares!==undefined){
                                    scope.lugares(results);
                                }
                            } else {
                              alert('No se ha podido encontrar la ubicación indicada: ' + status);
                              //window.history.back();
                            }
                    });
                }
            }
            
            function CrearMarcas(place){
                loc = place.geometry.location;
                positionMark.setPosition(new google.maps.LatLng(loc.lat(),loc.lng()));                
            }
            
            function updateMarkers() {                    
                    if (map) {                            
                        map.panTo(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                        positionMark.setPosition(new google.maps.LatLng(scope.center.lat,scope.center.lon));                        
                    }
            }
            
            function drawMarkers(){
                if(map!==undefined){
                    for(i in marksdrawedspoints){
                        marksdrawedspoints[i].setMap(null);
                    }

                    var marksdraweds = [];
                    for(var i in scope.markers){
                        l = scope.markers[i];
                        marksdraweds.push({location:new google.maps.LatLng(l.lat,l.lon),stopover:true});
                    }
                    if(marksdraweds.length>6){
                        marksdraweds.splice(6,2);
                    }                    
                    
                    if(marksdraweds.length==1){
                        marksdraweds[1] = marksdraweds[0];
                        marksdraweds[0] = {location:new google.maps.LatLng(scope.center.lat,scope.center.lon),stopover:true}                        
                    }
                    
                    if(marksdraweds.length>1){
                    directionsService.route({
                        origin: marksdraweds[0].location,
                        destination: marksdraweds[marksdraweds.length-1].location,
                        waypoints:marksdraweds,
                        travelMode: google.maps.TravelMode.DRIVING
                      }, function(response, status) {
                        if (status === google.maps.DirectionsStatus.OK) {
                          directionsDisplay.setDirections(response);
                           
                          for(i in marksdrawedspoints){
                                marksdrawedspoints[i].setMap(null);
                          }
                          for(var i in scope.markers){
                                var l = scope.markers[i];                                
                                var m =  new google.maps.Marker({ position: new google.maps.LatLng(l.lat,l.lon), map: map, title: l.name,icon:scope.markers[i].icon});
                                m.id = scope.markers[i].id;
                                m.datos = scope.markers[i];
                                marksdrawedspoints.push(m);
                            }
                          
                        } else {
                          window.alert('No se ha podido encontrar una ruta');
                        }
                      });
                    }
                }
            }

            // convert current location to Google maps location
            function getLocation(loc) {
                    if (loc == null) return new google.maps.LatLng(40, -73);
                    if (angular.isString(loc)) loc = scope.$eval(loc);
                    return new google.maps.LatLng(loc.lat, loc.lon);
                }

            } // end of link:
            
    }; // end of return
});