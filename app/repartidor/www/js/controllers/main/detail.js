controllers.controller('Detail', function($scope,$state,$http,$stateParams,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.urlServer = urlServer;
    Api.list('ventas',{id:$stateParams.id},$scope,$http,function(data){
        $scope.ventas = data[0];
        $scope.ventas.statusText = $scope.ventas.status==='2'?'Recogiendo': $scope.ventas.status==='3'?'En transito':'Completado';
        $scope.ventas.productos = JSON.parse($scope.ventas.productos);
        $scope.ventas.cliente = JSON.parse($scope.ventas.cliente);
        $scope.ventas.cliente.foto = typeof($scope.ventas.cliente.foto)!=='undefined' && $scope.ventas.cliente.foto!=='undefined'?urlServer+'/img/clientes/'+$scope.ventas.cliente.foto:'img/vacio.png';                    
        console.log($scope.ventas);
    });
});