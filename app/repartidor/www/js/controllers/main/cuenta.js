controllers.controller('Cuenta', function($rootScope,$scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.urlServer = urlServer;
    $scope.datos = {
        id:$scope.user,
        email:$scope.email,
        password:$scope.password,
        telefono:$scope.telefono!=='undefined'?$scope.telefono:'',
        celular:$scope.celular!=='undefined'?$scope.celular:'',
        foto: $scope.foto!=='undefined'?$scope.foto:'',
        repartidores_nombre:$scope.nombre
    };
    
    $scope.field_types = {
        password:'password'
    };
    
    $scope.send = function(datos){        
        $scope.data = datos;        
        Api.update('repartidores',$scope.user,$scope,$http,function(data){
            if(data.success){
                $scope.data.id = $scope.user;
                User.setData($scope.data);                
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Sus datos','Sus datos han sido actualizados con éxito');                
            }
        });
    };
    
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
    
    $scope.triggerFile = function(){        
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: 'Elige una opción para seleccionar la imagén',
          title: 'Dispositivo Multimedia',
          subTitle: 'Elige una opción',
          scope: $scope,
          buttons: [
            {
              text: '<b>Camara</b>',              
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.CAMERA);
                  myPopup.close();
              }
            },
            {
              text: '<b>Galería</b>',
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.SAVEDPHOTOALBUM);
                  myPopup.close();
              }
            }
          ]
        });
        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });
    };
    
    $scope.triggerDevice = function(sourceType){
        navigator.camera.getPicture(function(imageURI){
            $scope.selectImage(imageURI);            
        },
        function(message) { $scope.showAlert('get picture failed'); },
        {
            quality: 50, 
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: sourceType,            
            targetWidth: 284, 
            targetHeight: 284,
            allowEdit: true
        }
        );
    };
    
    $scope.selectImage = function(imageURI){
        $scope.data = {repartidores_id:$scope.user, foto:imageURI};
        Api.query('uploadPhotoRepartidor',$scope,$http,function(data){
            $scope.showAlert('Foto subida','Su foto ha sido guardada con éxito');
            $scope.datos.foto = data.trim();
            localStorage.foto = data.trim();
            $rootScope.$broadcast('reload');
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });
    };
});