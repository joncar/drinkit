controllers.controller('Socket', function($rootScope,$scope,$ionicPopup,$ionicPlatform,$ionicLoading,$ionicSideMenuDelegate,$state,User,$http,Api,UI,getDirFromGPS,appKeepRunning,pushGcm) {
    $scope = User.getData($scope);
    $scope.foto = $scope.foto!=='' && $scope.foto!=='undefined'?urlServer+'/img/fotos/'+$scope.foto:'img/vacio.png';
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
        navigator.app.exitApp();
    };
    
    //Reloj
    $rootScope.$on('updateReloj',function(evt,gps){
        clearTimeout($scope.relojTimer);
        gps = gps || true;
        $scope.reloj(gps);
    });    
    $scope.reloj = function(updateGps){
        updateGps = updateGps || true;
        if(updateGps){
            //Enviar coordenadas
            getDirFromGPS.getDirFromGps(function(lat,lon,dir){                
                localStorage.lat = lat;
                localStorage.lon = lon;
                $scope.getData();
            });
        }else{
            $scope.getData();
        }                 
    };
    $scope.getData = function(){        
            $scope.data = {repartidores_id:$scope.user,lat:localStorage.lat,lon:localStorage.lon};            
            Api.query('getUpdates',$scope,$http,function(data){            
                //Si no esta en el main y hay una venta pendiente se va para esa pantalla
               if(data.task.length > 0 && data.task[0].status==='2' && $state.current.name!=='tab.main'){
                     $state.go('tab.main');
               }
               $scope.status = data.repartidor.status==='2' || data.repartidor.status==='3'?true:false;               
               $rootScope.$broadcast('updateTask',data.task);
               $scope.relojTimer = setTimeout($scope.reloj,10000);
               if(!$scope.$$phase){
                   $scope.$apply();
               }
            });                    
    };
    
    $scope.status = true;
    $scope.bindStatus = function(status){        
        $scope.data = {status:status?2:1};
        Api.update('repartidores',$scope.user,$scope,$http,function(data){
            if(data.success){
                var msj = $scope.data.status==2?'Ahora estas disponible para recibir solicitudes':'Estas desconectado, por lo que no recibiras nuevas solicitudes';
                $scope.showAlert('Cambio de status',msj);
                $rootScope.$broadcast('updateReloj');
            }
        });
    };
    $scope.reloj();
    
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type === Connection.NONE) {
                $scope.showAlert('¿Haz perdido la conexión a internet?, necesitas conectarte para poder seguir usando la aplicación');
            }
        }
        appKeepRunning.init('Conectado');
        pushGcm.init($scope);
    });
    
    $rootScope.$on('reload',function(){
        $scope = User.getData($scope);
        $scope.foto = $scope.foto!=='' && $scope.foto!=='undefined'?urlServer+'/img/fotos/'+$scope.foto:'img/vacio.png';
    });
});