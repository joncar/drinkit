controllers.controller('Task', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    
    $scope.refresh = function(){
        Api.list('ventas',{repartidores_id:$scope.user},$scope,$http,function(data){
            for(var i in data){                
                data[i].statusText = data[i].status==='2'?'Recogiendo':data[i].status==='3'?'En tránsito':'Completado';
            }
            console.log(data);
            $scope.lista = data;            
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    $scope.refresh();    
});