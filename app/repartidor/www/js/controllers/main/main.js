var myPopup = [];
var controllers = angular.module('starter.controllers',['ionic']);
localStorage.innerHeight = (parseInt(window.innerHeight)-80)+'px';
controllers.controller('Main', function($rootScope,$scope,$ionicModal,$stateParams,$http,Querys,Api,UI,$ionicPopup,User,Api,Group,Querys) {  
    $scope.urlServer = urlServer;
    $scope.iconMap = 'http://74.208.77.223/img/fooddeliveryservice.png';
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.markers = [];
    $scope.innerheight = localStorage.innerHeight;
    if(typeof(localStorage.lat)==='undefined'){
        localStorage.lat = '47.55633987116614';
        localStorage.lon = '7.576619513223015';
    }
    $scope.basel = {lat:localStorage.lat,lon:localStorage.lon};
    $scope.task = [];
    $rootScope.$on('updateTask',function(evt,data){
        $scope.task = data;
        $scope.markers = [];
        for(var i in $scope.task){
            $scope.markers.push({lat:$scope.task[i].lat,lon:$scope.task[i].lon,name:'Venta #'+$scope.task[i].id});
        }
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    $rootScope.$broadcast('updateReloj');
    
    //Controles
    $scope.start = function(){
        $scope.data = {status:3,clientes_id:$scope.task[0].clientes_id};
        Api.update('ventas',$scope.task[0].id,$scope,$http,function(data){
            $scope.showAlert('Notificación','Mensaje enviado al cliente.');
            $rootScope.$broadcast('updateReloj',false);
        });
    };
    
    $scope.showDirection = function(){
      $scope.showAlert('Ubicación','<p><b>Cliente: </b>'+$scope.task[0].cliente+'</p><p><b>Dirección: </b>'+$scope.task[0].direccion_envio+'</p>');  
    };
    
    $scope.openNavigation = function(){
        window.open('http://maps.google.com/maps?q='+$scope.task[0].lat+','+$scope.task[0].lon, '_system');
    };
    
    $scope.complete = function(){
       $scope.confirm = UI.getConfirmBox($ionicPopup);
       $scope.confirm('¿Estás seguro?','¿Seguro que deseas completar esta orden?',function(){
           $scope.data = {clientes_id:$scope.task[0].clientes_id, status:4};
           Api.update('ventas',$scope.task[0].id,$scope,$http,function(data){
               if(data.success){
                    $scope.showAlert('Su envio ha sido entregado con éxito');
                    $rootScope.$broadcast('updateReloj');
                }else{
                    $scope.showAlert('Ocurrio un error en el sistema, reintente su acción');
                }
           });
       });
    };
});