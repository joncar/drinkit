<?php

	$this->set_css($this->default_theme_path.'/bootstrap/bootstrap/css/bootstrap.css');
        $this->set_css($this->default_theme_path.'/bootstrap/css/flexigrid.css');
        $this->set_js($this->default_theme_path.'/bootstrap/bootstrap/js/bootstrap.js');
	$this->set_js_lib($this->default_theme_path.'/bootstrap/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/bootstrap/js/flexigrid-add.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
        <?php get_instance()->load->view('notas',array('unset_back_to_list'=>$this->unset_back_to_list,'fields'=>$fields,'input_fields'=>$input_fields,'hidden_fields'=>$hidden_fields)); ?>
        <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
        <div id='report-error' style="display:none" class='alert alert-danger'></div>
        <div id='report-success' style="display:none" class='alert alert-success'></div>
        <div class="btn-group">			
            <button id="form-button-save" type='submit' class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
            <?php if(!$this->unset_back_to_list) { ?>
            <button type='button' id="save-and-go-back-button"  class="btn btn-default"><?php echo $this->l('form_save_and_go_back'); ?></button>                            
            <button type='button' id="cancel-button"  class="btn btn-danger"><?php echo $this->l('form_cancel'); ?></button>
            <?php } ?>		    
        </div> 
    <?php echo form_close(); ?>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
        $("input[type='text'],input[type='password']").addClass('form-control');
</script>